name := "forhonor-gate"

version := "0.25.0"

scalaVersion := "2.13.1"

cancelable in Global := true

turbo in ThisBuild := true

resolvers ++= Seq(Resolver.jcenterRepo)

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "net.logstash.logback" % "logstash-logback-encoder" % "6.1",

  "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8",

  "org.typelevel" %% "cats-core" % "2.0.0",

  "com.chuusai" %% "shapeless" % "2.3.3",

  "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf",

  "com.softwaremill.macwire" %% "macros" % "2.3.3" % "provided",

  "io.circe" %% "circe-core" % "0.12.3",
  "io.circe" %% "circe-generic" % "0.12.3",
  "io.circe" %% "circe-parser" % "0.12.3",
  "de.heikoseeberger" %% "akka-http-circe" % "1.30.0",

  "com.typesafe.akka" %% "akka-slf4j" % "2.6.1",

  "com.typesafe.akka" %% "akka-actor" % "2.6.1",
  "com.typesafe.akka" %% "akka-actor-typed" % "2.6.1",
  "com.typesafe.akka" %% "akka-persistence-typed" % "2.6.1",
  "com.typesafe.akka" %% "akka-stream" % "2.6.1",
  "com.typesafe.akka" %% "akka-http" % "10.1.11",
  "com.typesafe.akka" %% "akka-http-caching" % "10.1.11",
  "com.typesafe.akka" %% "akka-http2-support" % "10.1.11",

  "org.mongodb.scala" %% "mongo-scala-driver" % "2.8.0",
  "io.netty" % "netty-all" % "4.1.36.Final",

  "org.scalatest" %% "scalatest" % "3.0.8" % Test,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % "2.6.1" % Test
)

dependencyOverrides += "com.typesafe.akka" %% "akka-discovery" % "2.6.1"

Compile / scalacOptions += "-Ymacro-annotations"

enablePlugins(JavaAgent)
enablePlugins(AkkaGrpcPlugin)

akkaGrpcGeneratedSources := Seq(AkkaGrpc.Server)
akkaGrpcGeneratedLanguages := Seq(AkkaGrpc.Scala)
akkaGrpcCodeGeneratorSettings += "server_power_apis"

javaAgents += "org.mortbay.jetty.alpn" % "jetty-alpn-agent" % "2.0.9" % "runtime"

enablePlugins(JavaAppPackaging)
