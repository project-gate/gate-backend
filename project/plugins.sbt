// grpc
addSbtPlugin("com.lightbend.akka.grpc" % "sbt-akka-grpc" % "0.7.3")
addSbtPlugin("com.lightbend.sbt" % "sbt-javaagent" % "0.1.4")
// deploy
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.20")
// dotty
addSbtPlugin("ch.epfl.lamp" % "sbt-dotty" % "0.3.4")
