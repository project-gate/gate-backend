package me.seroperson.gate.forhonor

import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.model.{Assistant, Game, Hero, Qualifier, Stat}

import scala.concurrent.ExecutionContext

object HandyThings {

  implicit val ec = scala.concurrent.ExecutionContext.global

  private def printIntKeys(predicate: (Stat, Hero, Game, Assistant, Qualifier) => Boolean) =
    InformationChangeParsed
      .findKeysByPredicate(predicate)
      .foreach { keys =>
        println(keys.mkString(","))
      }

  private def printStringKeys(predicate: (Stat, Hero, Game, Assistant, Qualifier) => Boolean) =
    InformationChangeParsed
      .findKeysByPredicate(predicate)
      .flatMap { keys =>
        Main.informationKeyMapping.map { information =>
          keys.flatMap { key =>
            information.get(key)
          }
        }
      }
      .foreach { keys =>
        println(keys.map(_.hashCode).mkString(","))
        println(
          keys
            .map { key =>
              s"$key - ${key.hashCode}"
            }
            .mkString("\n")
        )
      }

  def printRankedDuelGameKeys()(implicit ec: ExecutionContext) =
    printIntKeys { case (stat, hero, game, assistant, qualifier) =>
      stat.isGameCompletedCountResult && game.isRankedDuel && qualifier.isPvp
    }

  def printDuelGameKeys()(implicit ec: ExecutionContext) =
    printStringKeys { case (stat, hero, game, assistant, qualifier) =>
      stat.isGameCompletedCountResult && game.isDuel && qualifier.isPvp
    }

  def printBrawlGameKeys()(implicit ec: ExecutionContext) =
    printStringKeys { case (stat, hero, game, assistant, qualifier) =>
      stat.isGameCompletedCountResult && game.isBrawl && qualifier.isPvp
    }

  def printDominionGameKeys()(implicit ec: ExecutionContext) =
    printStringKeys { case (stat, hero, game, assistant, qualifier) =>
      stat.isGameCompletedCountResult && game.isDominion && qualifier.isPvp
    }

  def printBreachGameKeys()(implicit ec: ExecutionContext) =
    printStringKeys { case (stat, hero, game, assistant, qualifier) =>
      stat.isGameCompletedCountResult && game.isBreach && qualifier.isPvp
    }

  def printArcadeGameKeys()(implicit ec: ExecutionContext) =
    printStringKeys { case (stat, hero, game, assistant, qualifier) =>
      stat.isGameCompletedCountResult && game.isArcade
    }

  def printGameKeys()(implicit ec: ExecutionContext) =
    printIntKeys { case (stat, hero, game, assistant, qualifier) =>
      stat.isGameCompletedCountResult && qualifier.isPvp && (game.isBrawl || game.isDuel || game.isDominion || game.isRankedDuel || game.isBreach)
    }

  def findStatKeyByHash(hash: Int) =
    Main.informationKeyMapping
      .map { information =>
        information.get(hash)
      }
      .foreach {
        _ match {
          case Some(value) =>
            println(value)
          case None =>
            println("Unknown hash")
        }
      }

}
