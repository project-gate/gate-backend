package me.seroperson.gate.forhonor

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.grpc.scaladsl.MetadataImpl
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.model.Uri.Path
import akka.http.scaladsl.model.Uri.Path.Segment
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Framing, Sink, StreamConverters}
import akka.util.{ByteString, Timeout}
import com.mongodb.{Block, ConnectionString}
import io.netty.channel.nio.NioEventLoopGroup
import me.seroperson.gate.forhonor.behavior.RequestMaker
import me.seroperson.gate.forhonor.di.Gate
import me.seroperson.gate.forhonor.model.{CodecRegister, Platform}
import me.seroperson.gate.forhonor.service.{MainService, OgService}
import me.seroperson.gate.forhonor.util.ConfigInstance
import org.mongodb.scala.connection.{ConnectionPoolSettings, NettyStreamFactoryFactory, SslSettings}
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoCredential}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.jdk.CollectionConverters._
import scala.language.postfixOps

object Main extends App {

  val defaultActorSystem = ActorSystem("untyped-system")
  val defaultMaterializer = ActorMaterializer()(defaultActorSystem)
  val defaultExecutionContext = defaultActorSystem.dispatcher
  val defaultScheduler = defaultActorSystem.scheduler

  val daoExecutionContext = defaultActorSystem.dispatchers.lookup("dao-dispatcher")

  implicit val globalTimeout = Timeout(30 seconds)

  val informationKeyMapping =
    StreamConverters.fromInputStream(() => getClass.getClassLoader.getResourceAsStream("keys.txt"))
      .via(Framing.delimiter(ByteString("\n"), 4096))
      .map(_.utf8String)
      .runWith(Sink.seq)(defaultMaterializer)
      .map { keys =>
        keys
          .map { key =>
            key.hashCode() -> key
          }
          .toMap
      }(defaultExecutionContext)

  val defaultDb = {

    val eventLoopGroup = new NioEventLoopGroup()

    defaultActorSystem.whenTerminated
      .map { _ =>
        eventLoopGroup.shutdownGracefully().await()
      }(defaultExecutionContext)

    val mongoSettingsBuilder = MongoClientSettings.builder()
      .streamFactoryFactory(
        NettyStreamFactoryFactory.builder()
          .eventLoopGroup(eventLoopGroup)
          .build()
      )
      .applyToSslSettings(new Block[SslSettings.Builder] {
        override def apply(t: SslSettings.Builder): Unit = {
          t
            .enabled(false)
            .invalidHostNameAllowed(true)
            .build()
        }
      })
      .applyToConnectionPoolSettings(new Block[ConnectionPoolSettings.Builder] {
        override def apply(t: ConnectionPoolSettings.Builder): Unit = {
          t
            .maxWaitTime(4, TimeUnit.MINUTES) // default * 2
            .maxConnectionIdleTime(5, TimeUnit.MINUTES)
            .maxWaitQueueSize(1500) // default * 3
            .maxSize(300) // default * 3
            .build()
        }
      })
      .applyConnectionString(new ConnectionString(ConfigInstance.MongoDb.getString("uri")))
      .codecRegistry(CodecRegister.allRegistries)

    val mongoSettings = (if(ConfigInstance.MongoDb.hasPath("credential")) {
      mongoSettingsBuilder
        .credential(
          MongoCredential.createCredential(
            ConfigInstance.MongoDb.getString("credential.username"),
            ConfigInstance.MongoDb.getString("credential.db"),
            ConfigInstance.MongoDb.getString("credential.password").toCharArray
          )
        )
    } else {
      mongoSettingsBuilder
    }).build()

    MongoClient(mongoSettings)
      .getDatabase(ConfigInstance.MongoDb.getString("db"))
  }

  val platforms = ConfigInstance
    .UbiServices
    .getConfigList("platforms")
    .asScala
    .map { element =>
      Platform(
        element.getString("name"),
        element.getString("id"),
        element.getString("space-id"),
        element.getString("sandbox-id")
      )
    }
    .toList

  val gate = new Gate(defaultDb)

  val http = {
    def extractPathFromMetadata(
      service: String,
      platformHandlers: Map[String, PartialFunction[HttpRequest, Future[HttpResponse]]]
    ) =
      Function
        .unlift[HttpRequest, Future[HttpResponse]](req =>
          req.uri.path match {
            case Path.Slash(Segment(`service`, Path.Slash(Segment(method, Path.Empty)))) =>
              for {
                platformHeader <- new MetadataImpl(req.headers).getText("platform")
                platformHandler <- platformHandlers.get(platformHeader)
              } yield platformHandler.apply(req)
            case _ =>
              // idk wtf is that request
              None
          }
        )

    val servicesPlatformHandlers = platforms
      .map { platform =>
        platform.name -> gate.initServices(platform)
      }
      .toMap
    val ogHandlers = servicesPlatformHandlers
      .map { case (platform, (_, og)) =>
        platform -> og
      }
    val mainHandlers = servicesPlatformHandlers
      .map { case (platform, (main, _)) =>
        platform -> main
      }

    val ogService = extractPathFromMetadata(OgService.name, ogHandlers)
    val mainService = mainHandlers
      .foldLeft(PartialFunction.empty[HttpRequest, Future[HttpResponse]]) { case (acc, (platform, platformedHandler)) =>
        // trying to handle /platform/MainService/method by transforming it into /MainService/method
        // and passing into associated handler
        acc.orElse(
          Function
            .unlift[HttpRequest, HttpRequest](req => req.uri.path match {
              case Path.Slash(Segment(`platform`, Path.Slash(Segment(MainService.name, Path.Slash(Segment(method, Path.Empty)))))) ⇒
                Some(
                  req.copy(
                    uri = req.uri.copy(
                      path = Path.Slash(Segment(MainService.name, Path.Slash(Segment(method, Path.Empty))))
                    )
                  )
                )
              case _ =>
                None
            })
            .andThen(platformedHandler)
        )
      }
      .orElse[HttpRequest, Future[HttpResponse]](
        // if we are failed to handle /platform/*
        // then trying to handle /MainService/method assuming that platform is stored within metadata
        // if any -> invoking associated platform handler
        extractPathFromMetadata(MainService.name, mainHandlers)
      )

    val serverHost = ConfigInstance.Http.getString("server.host")
    val serverPort = ConfigInstance.Http.getInt("server.port")

    Http()(gate.applicationComponents.httpActorSystem)
      .bindAndHandleAsync(
        mainService.orElse(ogService),
        interface = serverHost,
        port = serverPort
      )(gate.applicationComponents.httpMaterializer)
  }

  gate.requestMakerActor ! RequestMaker.RequestAuthorization

}
