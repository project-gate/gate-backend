package me.seroperson.gate.forhonor.behavior

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed._
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.{InformationChange, Player}
import me.seroperson.gate.forhonor.util.ConfigInstance
import shapeless.tag.@@

import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters._

class ChangesReplicator(
  webhookHuckster: ActorRef[WebhookHuckster.Command],
  executionContext: ExecutionContext @@ Tags.Behaviour
) {

  implicit val ec = executionContext

  def behavior: Behavior[(Player, Seq[InformationChange])] = Behaviors.setup { ctx =>
    val profileIds = ConfigInstance
      .Webhook
      .Root
      .getStringList("enabled-for-profiles")
      .asScala
    Behaviors.receive[(Player, Seq[InformationChange])] { (ctx, msg) =>
      val (player, changes) = msg
      if(profileIds.contains(player.profile.id)) {
        Future.sequence(changes.map(_.toParsed())).map { parsedChanges =>
          webhookHuckster ! WebhookHuckster.ReceiveChanges(
            player.name,
            player.profile.id,
            parsedChanges.flatten.toSeq
          )
        }
      }
      Behaviors.same
    }
  }

}
