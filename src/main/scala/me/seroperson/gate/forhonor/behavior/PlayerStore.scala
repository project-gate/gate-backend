package me.seroperson.gate.forhonor.behavior

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.caching.scaladsl.Cache
import akka.stream.ActorMaterializer
import akka.util.Timeout
import me.seroperson.gate.forhonor.behavior.PlayerStore._
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed._
import me.seroperson.gate.forhonor.behavior.model.LeaderboardChangeExtended._
import me.seroperson.gate.forhonor.dao.{HistoryDaoService, LeaderboardDaoService, PlayerDaoService}
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.cache.GraphCacheKey
import me.seroperson.gate.forhonor.model.{Game, Platform, Player, Season}
import me.seroperson.gate.forhonor.service.{PlayerCustomLeaderboard, PlayerCustomLeaderboardByGame, PlayerCustomLeaderboardElement, PlayerRateGraph, PlayerRateGraphByGame, PlayerRateGraphByHero, SharedHelper}
import me.seroperson.gate.forhonor.util.ConfigInstance
import shapeless.tag.@@

import concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

/**
 * Actor holds the list of current users.
 */
class PlayerStore(
  platform: Platform,
  requestMaker: ActorRef[RequestMaker.Command],
  playerDaoService: PlayerDaoService,
  leaderboardDaoService: LeaderboardDaoService,
  historyDaoService: HistoryDaoService,
  perSeasonCustomLeaderboardCache: Cache[GraphCacheKey, (PlayerRateGraph, PlayerCustomLeaderboard)],
  materializer: ActorMaterializer @@ Tags.Behaviour,
  executionContext: ExecutionContext @@ Tags.Behaviour,
  timeout: Timeout @@ Tags.DefaultTimeout
) extends SharedHelper {

  implicit val mat = materializer
  implicit val ec = executionContext
  implicit val t = timeout

  private val leaderboardQueryPeriod =
    Duration.fromNanos(ConfigInstance.Gate.TrackerLeaderboard.getDuration("query-period").toNanos)
  private val leaderboardQueryAdjustmentPeriod =
    Duration.fromNanos(ConfigInstance.Gate.TrackerLeaderboard.getDuration("query-adjustment-period").toNanos)
  private val isLeaderboardEnabled =
    ConfigInstance.Gate.TrackerLeaderboard.getBoolean("enabled")

  private val namesQueryPeriod =
    Duration.fromNanos(ConfigInstance.Gate.TrackerNames.getDuration("query-period").toNanos)
  private val isNamesEnabled =
    ConfigInstance.Gate.TrackerNames.getBoolean("enabled")

  private val graphQueryPeriod =
    Duration.fromNanos(ConfigInstance.Gate.TrackerGraph.getDuration("query-period").toNanos)
  private val isGraphEnabled =
    ConfigInstance.Gate.TrackerGraph.getBoolean("enabled")

  def behavior: Behavior[Command] =
    Behaviors.withTimers { scheduler =>
      scheduler.startSingleTimer(LeaderboardTimerKey, RequestLeaderboard, 10 seconds)

      scheduler.startSingleTimer(NamesInitialTimerKey, RequestNames, 10 seconds)
      scheduler.startPeriodicTimer(NamesPeriodicTimerKey, RequestNames, namesQueryPeriod)

      scheduler.startSingleTimer(GraphInitialTimerKey, UpdateGraph, 10 seconds)
      scheduler.startPeriodicTimer(GraphPeriodicTimerKey, UpdateGraph, graphQueryPeriod)

      Behaviors.setup { ctx =>
        val leaderboardAdapter = ctx.messageAdapter[List[String]] { v =>
          TrackNewLeaderboardPlayersAndStore(v)
        }
        val storePlayersAdapter = ctx.messageAdapter[Set[Player]] { v =>
          StorePlayers(v)
        }
        val updatePlayersAdapter = ctx.messageAdapter[Set[Player]] { v =>
          UpdatePlayers(v)
        }
        commandHandler(
          State(
            List.empty,
            true,
            false
          ),
          leaderboardAdapter,
          storePlayersAdapter,
          updatePlayersAdapter,
          scheduler,
          ctx
        )
      }
    }

  private case class State(
    lastLeaderboard: List[String],
    isFirstLeaderboardRequest: Boolean,
    updatingGraph: Boolean
  )

  private def commandHandler(
    state: State,
    leaderboardAdapter: ActorRef[List[String]],
    storePlayerAdapter: ActorRef[Set[Player]],
    updatePlayerAdapter: ActorRef[Set[Player]],
    schedulerTyped: TimerScheduler[Command],
    ctx: ActorContext[Command]
  ): Behavior[Command] = Behaviors.withMdc(Map("platform" -> platform.toString))(Behaviors.receive {

      case (ctx, TrackNewLeaderboardPlayersAndStore(leaderboard)) =>
        playerDaoService.findByProfileIds(leaderboard)
          .map { tracked =>
            val untracked = leaderboard.diff(tracked.map(_.profile.id))
            val untrackedPlayers = untracked.grouped(50)
            untrackedPlayers.foreach { slice =>
              requestMaker ! RequestMaker.ProfileIdsByIds(
                platform,
                slice,
                Player.SourceLeaderboardAutopopulation,
                storePlayerAdapter
              )
            }
          }
          .recover {
            case ex =>
              ctx.log.error("Exception during PlayerStore.RequestLeaderboard.findByProfileIds")
              ex.printStackTrace()
          }
        ctx.self ! StoreLeaderboard(leaderboard)
        Behaviors.same

      case (ctx, RequestLeaderboard) =>
        if(isLeaderboardEnabled) {
          requestMaker ! RequestMaker.Leaderboard(platform, leaderboardAdapter)
        }
        Behaviors.same

      case (ctx, RequestNames) =>
        if(isNamesEnabled) {
          playerDaoService
            .findAll()
            .map { players =>
              players
                .grouped(50)
                .foreach { groupedPlayers =>
                  requestMaker ! RequestMaker.ProfileIdsByIds(
                    platform,
                    groupedPlayers.map(_.profile.id),
                    -1,
                    updatePlayerAdapter
                  )
                }
            }
        }
        Behaviors.same

      case (ctx, UpdateGraph) =>

        if(isGraphEnabled) {

          ctx.log.info("Updating player graphs")

          val allSeasons = Season.AllSeasons.getOrElse(platform.name, List.empty)
          val lastSeason = allSeasons.lastOption

          val existingKeys = perSeasonCustomLeaderboardCache
            .keys
            .map(_.season)

          val allPlayers = playerDaoService.findAll()

          def dirtyRound(value: Double) = math.round(value * 100.0) / 100.0

          def updateSeason(season: Season): Future[(PlayerRateGraph, PlayerCustomLeaderboard)] = {
            for {
              players <- allPlayers
              playerElements = playersToPlayerElements(players)
              rateGraph <- historyDaoService.findLiveCaptured(season.startTimestamp, season.endTimestamp(allSeasons))
              flattenRateGraph = rateGraph
                .flatMap { case (playerId, timestampChanges) =>
                  players
                    .find(_.id == playerId)
                    .map { player =>
                      timestampChanges
                        .map { case (timestamp, changes) =>
                          (player, timestamp, changes)
                        }
                    }
                    .getOrElse(Iterable.empty)
                }
              flattenWithPlayerRateGraph = flattenRateGraph
                .flatMap { case (player, _, changes) =>
                  changes
                    .map(player -> _)
                }
              flattenGroupedByGame = flattenWithPlayerRateGraph
                .filter { case (_, change) =>
                  pvpPredicate(change) &&
                    (change.game.isRankedDuel ||
                      change.game.isDuel ||
                      change.game.isDominion ||
                      change.game.isBrawl ||
                      change.game.isBreach)
                }
                .groupBy { case (_, change) =>
                  change.game
                }
            } yield {
              (
                PlayerRateGraph(
                  flattenGroupedByGame
                    .map { case (game, withPlayerByGameGraph) =>
                      val byGameGraph = withPlayerByGameGraph.map { case (_, changes) =>
                        changes
                      }
                      val totalGames = fetchStatValue(byGameGraph)(gameCountPredicate)
                      val totalKills = fetchStatValue(byGameGraph)(humanKillPredicate)
                      val totalDeaths = fetchStatValue(byGameGraph)(deathPredicate)
                      PlayerRateGraphByGame(
                        game,
                        byGameGraph
                          .groupBy(_.hero)
                          .map { case (hero, byHeroGraph) =>
                            val games = fetchStatValue(byHeroGraph)(gameCountPredicate)
                            val kills = fetchStatValue(byHeroGraph)(humanKillPredicate)
                            val deaths = fetchStatValue(byHeroGraph)(deathPredicate)
                            PlayerRateGraphByHero(
                              hero,
                              games,
                              kills,
                              deaths,
                              (if (deaths == 0) {
                                0.0
                              } else {
                                dirtyRound(kills.toDouble / deaths.toDouble)
                              }),
                              (if (totalGames == 0) {
                                0
                              } else {
                                dirtyRound((games.toDouble / totalGames.toDouble * 100.0).toDouble)
                              })
                            )
                          }
                          .toSeq
                          .sortBy(_.games * -1)
                      )
                    }
                    .toSeq
                    .sortBy(value => graphGameOrdering.getOrElse(value.game, -1))
                ),
                PlayerCustomLeaderboard(
                  flattenGroupedByGame
                    .map { case (game, withPlayerByGameGraph) =>
                      val byGameGraph = withPlayerByGameGraph.map { case (_, changes) =>
                        changes
                      }
                      val totalGames = fetchStatValue(byGameGraph)(gameCountPredicate)
                      val totalKills = fetchStatValue(byGameGraph)(humanKillPredicate)
                      val totalDeaths = fetchStatValue(byGameGraph)(humanDeathPredicate)
                      PlayerCustomLeaderboardByGame(
                        game,
                        withPlayerByGameGraph
                          .groupBy { case (player, _) =>
                            player
                          }
                          .map { case (player, changes) =>
                            val rawChanges = changes.map { case (_, rawChange) =>
                              rawChange
                            }
                            val games = fetchStatValue(rawChanges)(gameCountPredicate)
                            val kills = fetchStatValue(rawChanges)(humanKillPredicate)
                            val deaths = fetchStatValue(rawChanges)(humanDeathPredicate)
                            PlayerCustomLeaderboardElement(
                              Some(playerToPlayerElement(player)),
                              games,
                              kills,
                              deaths,
                              (if (deaths == 0) {
                                0.0
                              } else {
                                dirtyRound(kills.toDouble / deaths.toDouble)
                              })
                            )
                          }
                          .toSeq
                          .sortBy(_.games * -1)
                          .take(10)
                      )
                    }
                    .toSeq
                    .sortBy(value => graphGameOrdering.getOrElse(value.game, -1))
                )
              )
            }
          }

          allSeasons
            .foldLeft(Future.unit) { case (future, season) =>
              val isLastSeason = lastSeason.exists(_ == season)
              val previousSeason = season.previousSeason(allSeasons)
              val isCached = existingKeys.exists(_ == season)
              future.flatMap { _ =>
                if (!isCached || isLastSeason) {
                  val startTimestamp = System.currentTimeMillis()
                  ctx.log.info("Updating season {} ({})", season.number, platform.name)
                  updateSeason(season)
                    .flatMap { case (graph, customLeaderboard) =>
                      val endTimestamp = System.currentTimeMillis()
                      ctx.log.info("Completed updating for season {} in {}ms", season.number, endTimestamp - startTimestamp)

                      // collecting the diff from the previous season
                      (previousSeason match {
                        case Some(previousSeason) =>
                          perSeasonCustomLeaderboardCache
                            .get(GraphCacheKey(previousSeason))
                            .getOrElse(Future.successful((PlayerRateGraph(Seq.empty), PlayerCustomLeaderboard(Seq.empty))))
                            .map { case (previousGraph, _) =>
                              graph
                                .copy(
                                  graph = graph.graph.map { currentByGame =>
                                    val currentGame = currentByGame.game
                                    previousGraph.graph.find(_.game == currentGame) match {
                                      case Some(previousByGame) =>
                                        currentByGame
                                          .withGraph(
                                            currentByGame.graph.map { currentByHero =>
                                              val currentHero = currentByHero.hero
                                              previousByGame.graph.find(_.hero == currentHero) match {
                                                case Some(previousByHero) =>
                                                  currentByHero
                                                    .withRawPickRateDiff(dirtyRound(currentByHero.rawPickRate - previousByHero.rawPickRate))
                                                    .withPositionDiff(previousByGame.graph.indexOf(previousByHero) - currentByGame.graph.indexOf(currentByHero))
                                                case None =>
                                                  currentByHero
                                                    .withRawPickRateDiff(-999.0)
                                                    .withPositionDiff(-999)
                                              }
                                            }
                                          )
                                      case None =>
                                        currentByGame
                                          .withGraph(
                                            currentByGame.graph.map { currentByHero =>
                                              currentByHero
                                                .withRawPickRateDiff(-999.0)
                                                .withPositionDiff(-999)
                                            }
                                          )
                                    }
                                  }
                                )
                            }
                        case None =>
                          Future.successful(
                            graph
                              .withGraph(
                                graph.graph.map { currentByGame =>
                                  currentByGame
                                    .withGraph(
                                      currentByGame.graph.map { currentByHero =>
                                        currentByHero
                                          .withRawPickRateDiff(-999.0)
                                          .withPositionDiff(-999)
                                      }
                                    )
                                }
                              )
                          )
                      })
                        .flatMap { graphWithDiffs =>
                          perSeasonCustomLeaderboardCache
                            .put(GraphCacheKey(season), Future.successful((graphWithDiffs, customLeaderboard)))
                            .map(_ => ())
                        }
                    }
                } else {
                  Future.unit
                }
              }
            }
            .recover {
              case ex =>
                ctx.log.error("Exception during PlayerStore.UpdateGraph")
                ex.printStackTrace()
                ()
            }
            .map { result =>
              ctx.self ! FinishGraphUpdating
            }

          commandHandler(
            state.copy(
              updatingGraph = true
            ),
            leaderboardAdapter,
            storePlayerAdapter,
            updatePlayerAdapter,
            schedulerTyped,
            ctx
          )
        } else {
          ctx.log.info("Skipping graphs updating")

          Behaviors.same
        }

      case (ctx, FinishGraphUpdating) =>
        commandHandler(
          state.copy(
            updatingGraph = false
          ),
          leaderboardAdapter,
          storePlayerAdapter,
          updatePlayerAdapter,
          schedulerTyped,
          ctx
        )

      case (ctx, IsUpdatingGraph(responseTo)) =>
        responseTo ! state.updatingGraph

        Behaviors.same

      case (ctx, StoreLeaderboard(newLeaderboard)) =>
        Future.successful(state.lastLeaderboard)
          .flatMap { value =>
            if(value.isEmpty) {
              leaderboardDaoService.composeLeaderboard()
            } else {
              Future.successful(value)
            }
          }
          .map { currentLeaderboard =>
            val result = currentLeaderboard.diffWith(newLeaderboard)
            val nextPeriod = if(!state.isFirstLeaderboardRequest && result.nonEmpty) {
              leaderboardQueryPeriod
            } else {
              leaderboardQueryAdjustmentPeriod
            }
            schedulerTyped.startSingleTimer(
              LeaderboardTimerKey,
              RequestLeaderboard,
              nextPeriod
            )
            result
          }
          .flatMap { newDiff =>
            if(newDiff.nonEmpty) {
              ctx.log.info("Updating leaderboard with {} changes", newDiff.size)
              ctx.self ! PreserveLeaderboard(newLeaderboard)
              leaderboardDaoService
                .insertAll(newDiff)
                .map(_ => ())
            } else {
              ctx.log.info("Leaderboard was the same. Skipping")
              Future.successful(())
            }
          }
          .recover {
            case ex =>
              ctx.log.error("Exception during PlayerStore.StoreLeaderboard")
              ex.printStackTrace()
          }

        commandHandler(
          State(
            state.lastLeaderboard,
            false,
            state.updatingGraph
          ),
          leaderboardAdapter,
          storePlayerAdapter,
          updatePlayerAdapter,
          schedulerTyped,
          ctx
        )

      case (ctx, StorePlayers(players)) =>
        playerDaoService
          .insertAll(players.toSeq)
          .recover {
            case ex =>
              ctx.log.error("Exception during PlayerStore.StorePlayers.insertAll")
              ex.printStackTrace()
          }
        Behaviors.same

      case (ctx, UpdatePlayers(players)) =>
        playerDaoService
          .insertOrUpdate(players.toSeq)
          .recover {
            case ex =>
              ctx.log.error("Exception during PlayerStore.UpdatePlayers.insertOrUpdate")
              ex.printStackTrace()
          }
        Behaviors.same

      case (ctx, PreserveLeaderboard(newLeaderboard)) =>
        commandHandler(
          State(
            newLeaderboard,
            state.isFirstLeaderboardRequest,
            state.updatingGraph
          ),
          leaderboardAdapter,
          storePlayerAdapter,
          updatePlayerAdapter,
          schedulerTyped,
          ctx
        )

      case (ctx, FailedToFetchLeaderboard) =>
        schedulerTyped.startSingleTimer(
          LeaderboardTimerKey,
          RequestLeaderboard,
          leaderboardQueryAdjustmentPeriod
        )
        Behaviors.same
  })

}

object PlayerStore {
  private val graphGameOrdering = Map[Game, Int](
    Game.Dominion -> 0,
    Game.RankedDuel -> 1,
    Game.Duel -> 2,
    Game.Brawl -> 3,
    Game.Breach -> 4
  )

  private[behavior] object LeaderboardTimerKey
  private[behavior] object NamesInitialTimerKey
  private[behavior] object NamesPeriodicTimerKey

  private[behavior] object GraphInitialTimerKey
  private[behavior] object GraphPeriodicTimerKey

  sealed trait Command
  final case object RequestLeaderboard extends Command
  final case object RequestNames extends Command
  final case object UpdateGraph extends Command
  final case object FinishGraphUpdating extends Command

  private[behavior] final case class IsUpdatingGraph(response: ActorRef[Boolean]) extends Command

  private[behavior] final case class TrackNewLeaderboardPlayersAndStore(leaderboard: List[String]) extends Command
  private[behavior] final case class PreserveLeaderboard(leaderboard: List[String]) extends Command
  private[behavior] final case class StoreLeaderboard(leaderboard: List[String]) extends Command
  private[behavior] final case object FailedToFetchLeaderboard extends Command
  private[behavior] final case class StorePlayers(players: Set[Player]) extends Command
  private[behavior] final case class UpdatePlayers(players: Set[Player]) extends Command
}
