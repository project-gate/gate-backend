package me.seroperson.gate.forhonor.behavior

import akka.actor.Scheduler
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import me.seroperson.gate.forhonor.Main
import me.seroperson.gate.forhonor.behavior.PlayerTracker._
import me.seroperson.gate.forhonor.behavior.model.Shared
import me.seroperson.gate.forhonor.dao.{HistoryDaoService, InconsistencyDaoService, PlayerDaoService, UnknownKeyDaoService}
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model._
import me.seroperson.gate.forhonor.util.ConfigInstance
import org.mongodb.scala.bson.ObjectId
import shapeless.tag.@@

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration
import scala.jdk.CollectionConverters._

/**
 * Actor updates per-player stats.
 */
class PlayerTracker(
  platform: Platform,
  replicateTo: ActorRef[(Player, Seq[InformationChange])],
  playerStore: ActorRef[PlayerStore.Command],
  requestMaker: ActorRef[RequestMaker.Command],
  playerDaoService: PlayerDaoService,
  historyDaoService: HistoryDaoService,
  unknownKeyDaoService: UnknownKeyDaoService,
  inconsistencyDaoService: InconsistencyDaoService,
  materializer: ActorMaterializer @@ Tags.Behaviour,
  executionContext: ExecutionContext @@ Tags.Behaviour,
  schedulerClassic: Scheduler @@ Tags.Behaviour,
  timeout: Timeout @@ Tags.DefaultTimeout
) extends Shared {

  implicit val mat = materializer
  implicit val ec = executionContext
  implicit val sc = schedulerClassic
  implicit val t = timeout

  private val checkQueryPeriod = Duration.fromNanos(ConfigInstance.Gate.Tracker.getDuration("check-query-period").toNanos)
  private val startDelay = Duration.fromNanos(ConfigInstance.Gate.Tracker.getDuration("start-delay").toNanos)
  private val inactivityQueryPeriods =
    ConfigInstance
      .Gate
      .Tracker
      .getDurationList("inactivity-query-periods")
      .asScala
      .map { value =>
        Duration.fromNanos(value.toNanos)
      }
      .toList
      .zipWithIndex
      .map { case (x, y) =>
        y -> x
      }
      .toMap
  private val offlineQueryPeriod = Duration.fromNanos(ConfigInstance.Gate.Tracker.getDuration("offline-query-period").toNanos)
  private val deadQueryPeriod = Duration.fromNanos(ConfigInstance.Gate.Tracker.getDuration("dead-query-period").toNanos)
  private val deadIndicationPeriod = Duration.fromNanos(ConfigInstance.Gate.Tracker.getDuration("dead-indication-period").toNanos)
  private val isEnabled = ConfigInstance.Gate.Tracker.getBoolean("enabled")
  private val unknownKeyInspectionIsEnabled = ConfigInstance.Gate.Tracker.getBoolean("unknown-key-inspection-enabled")
  private val inconsistencyTrackingIsEnabled = ConfigInstance.Gate.Tracker.getBoolean("inconsistency-tracking-enabled")

  def behavior: Behavior[Command] =
    Behaviors.withTimers { scheduler =>
      scheduler.startSingleTimer(TimerKey, Update, startDelay)
      commandHandler(State(Set.empty, Set.empty, Set.empty), scheduler)
    }

  private case class State(
    playersStoringInProgress: Set[ObjectId],
    playersRequestInProgress: Set[ObjectId],
    playersRequestAwaits: Set[Player]
  )

  private def commandHandler(
    state: State,
    schedulerTyped: TimerScheduler[Command]
  ): Behavior[Command] = Behaviors.withMdc(Map("platform" -> platform.toString))(Behaviors.receive { (ctx, msg) =>

    val statisticsAdapter = ctx.messageAdapter[Map[Player, Iterable[(String, Double)]]] { m =>
     UpdatePlayerStats(m)
    }

    val updatingBridge = ctx.messageAdapter[Boolean] { isUpdatingNow =>
      ValidatedUpdate(!isUpdatingNow && isEnabled)
    }

    msg match {

      case Update =>
        playerStore ! PlayerStore.IsUpdatingGraph(updatingBridge)

        Behaviors.same

      case ValidatedUpdate(notSkip) =>
        if (notSkip) {
          playerDaoService
            .findAll()
            .recover {
              case ex =>
                ctx.log.error("Exception during PlayerTracker.Update.findAll")
                ex.printStackTrace()
                Seq.empty
            }
            .foreach { players =>
              val now = System.currentTimeMillis()
              val filteredByActivity =
                players
                  .filter { player =>
                    val fetchAt = {
                      val isDead = player.activity.lastActivity match {
                        case Some(lastActivity) =>
                          now >= lastActivity + deadIndicationPeriod.toMillis
                        case None =>
                          false
                      }
                      val lastActivityCheckO = player.activity.lastActivityCheck
                      val startPoint = lastActivityCheckO match {
                        case Some(lastActivityCheck) =>
                          lastActivityCheck
                        case None =>
                          0L // fetch immediately
                      }
                      if (isDead) {
                        startPoint + deadQueryPeriod.toMillis
                      } else {
                        val inactivityCombo = player.activity.inactivityCombo
                        val currentDelay = inactivityQueryPeriods
                          .get(inactivityCombo)
                        startPoint + currentDelay.getOrElse(offlineQueryPeriod).toMillis
                      }
                    }
                    now >= fetchAt
                  }
                  .sortBy(_.activity.lastActivityCheck)
              ctx.log.info("Going to check {}/{} players", filteredByActivity.size, players.size)
              schedulerTyped.startSingleTimer(TimerKey, Update, checkQueryPeriod)
              ctx.self ! Update(filteredByActivity)
            }
        } else {
          schedulerTyped.startSingleTimer(TimerKey, Update, checkQueryPeriod)
          ctx.log.info("Skipping update")
        }

        Behaviors.same

      case Update(players) =>
        val pendingRequestSize = state.playersRequestInProgress.size
        val totalAvailable = 150
        val (playersToRequest, playersToAwait) = if (pendingRequestSize < totalAvailable) {
          val requestCount = totalAvailable - pendingRequestSize
          (players.take(requestCount), players.drop(requestCount))
        } else {
          (Seq.empty, players)
        }

        val playersToReqestSize = playersToRequest.size
        val playersToAwaitSize = playersToAwait.size
        if (playersToReqestSize > 0 || playersToAwaitSize > 0) {
          val playersToAwaitText = if(playersToAwaitSize > 0) {
            "{} are awaiting because {} are still in-progress"
          } else {
            "nothing to await"
          }

          ctx.log.info(
            s"Sending requests to {} players, $playersToAwaitText",
            playersToReqestSize,
            playersToAwaitSize,
            pendingRequestSize
          )

          playersToRequest
            .grouped(50)
            .foreach { players =>
              requestMaker ! RequestMaker.Statistics(platform, players, statisticsAdapter)
            }

          commandHandler(
            state.copy(
              playersRequestInProgress = state.playersRequestInProgress ++ playersToRequest.map(_.id),
              playersRequestAwaits = state.playersRequestAwaits ++ playersToAwait
            ),
            schedulerTyped
          )
        } else {
          Behaviors.same
        }

      case UpdatePlayerStats(initial) =>
        val filteredResult = initial.filterNot { case (player, _) =>
          state.playersStoringInProgress.contains(player.id)
        }
        val playerIds = filteredResult.keys.map(_.id).toSeq
        val playerIdsSet = playerIds.toSet
        historyDaoService
          .findTotal(playerIds)
          .flatMap { total =>
            val byPlayerTotal = total
              .map { value =>
                value.playerId -> value.keys
              }
              .toMap
            val groupedByType = filteredResult
              .toList
              .map { case (player, incomingStats) =>
                val playerTotal = byPlayerTotal
                  .getOrElse(player.id, Seq.empty)
                  .map { value =>
                    value.key -> value.value
                  }
                  .toMap
                if (unknownKeyInspectionIsEnabled) {
                  Main
                    .informationKeyMapping
                    .flatMap { stored =>
                      val unknownKeys = incomingStats
                        .filterNot { case (incomingStat, _) =>
                          stored.contains(incomingStat.hashCode)
                        }
                        .map { case (unknownKey, _) =>
                          unknownKey
                        }
                        .toSet
                      unknownKeyDaoService.insertIfIsNotExists(unknownKeys)
                    }
                    .map { value =>
                      if (value > 0) {
                        ctx.log.info("Inserted {} new unknown keys", value)
                      }
                      value
                    }
                    .recover {
                      case ex =>
                        ctx.log.error("Exception during analyzing unknown keys", ex)
                    }
                }

                player -> incomingStats.flatMap { case (incomingStat, incomingValue) =>
                  // todo check if InformationKeyConverter has information about this incomingStat?
                  val storedValue = playerTotal.getOrElse(incomingStat.hashCode, 0.0)
                  val diff = incomingValue - storedValue
                  if (diff == 0.0) {
                    None
                  } else if (diff < 0.0) {
                    if (inconsistencyTrackingIsEnabled) {
                      Some(IndexDiffInconsistent, incomingStat -> diff * -1)
                    } else {
                      None
                    }
                  } else {
                    Some(IndexDiffRegular, incomingStat -> diff)
                  }
                }
              }
              .filter { case (_, changes) =>
                changes.nonEmpty
              }
              .toMap
              .view
              .mapValues {
                _
                  .groupBy { case (tpe, _) =>
                    tpe
                  }
                  .view
                  .mapValues {
                    _
                      .map { case (_, change) =>
                        change
                      }
                  }
                  .toMap
              }
              .toMap

            val timestamp = System.currentTimeMillis()

            def retrieveChanges(diffIndex: Int) = groupedByType
              .view
              .mapValues {
                _.getOrElse(diffIndex, Iterable.empty)
              }
              .filter { case (_, value) =>
                value.nonEmpty
              }
              .toMap

            val goingToStore = retrieveChanges(PlayerTracker.IndexDiffRegular)
            val inconsistent = if (inconsistencyTrackingIsEnabled) {
              retrieveChanges(PlayerTracker.IndexDiffInconsistent)
            } else {
              Map.empty
            }

            if (inconsistent.nonEmpty) {
              inconsistent
                .toList
                .foreach { case (player, changes) =>
                  historyDaoService
                    .findForInconsistency(
                      player.id,
                      changes
                        .map { case (key, value) =>
                          key.hashCode -> value
                        }
                        .toSeq
                    )
                    .flatMap { changes =>
                      inconsistencyDaoService.insertIfIsNotExists(
                        changes
                          .groupBy { change =>
                            change.key
                          }
                          .view
                          .mapValues { changes =>
                            changes
                              .sortBy(_.timestamp * -1)
                              .headOption
                              .map(_.id)
                          }
                          .values
                          .flatten
                          .toSeq
                      )
                    }
                    .map { count =>
                      if (count > 0) {
                        ctx.log.info("Saving {} inconsistency changes", count)
                      }
                      count
                    }
                    .recover {
                      case ex =>
                        ctx.log.error("Exception during updating inconsistency", ex)
                    }
                }
            }

            val result = if (goingToStore.nonEmpty) {
              ctx.log.info(
                "Going to store {} changes for {} players",
                goingToStore.values.map(_.size).sum,
                goingToStore.keys.map(_.name).mkString("[", ", ", "]")
              )
              goingToStore
                .toList
                .map { case (player, changes) =>
                  player -> changes
                    .map { case (stat, value) =>
                      InformationChange(
                        new ObjectId(),
                        player.id,
                        timestamp,
                        stat.hashCode(),
                        value,
                        player.activity.firstActivity.isEmpty
                      )
                    }
                    .toSeq
                }
                .toMap
            } else {
              // nothing changed for this player
              Map.empty[Player, Seq[InformationChange]]
            }

            val resultPlayers = result.keys.toSeq
            val allPlayers = filteredResult.keys
            val emptyPlayers = allPlayers
              .filterNot { player =>
                resultPlayers.contains(player)
              }
              .toSeq
            playerDaoService.updatePlayersActivity(resultPlayers, emptyPlayers, timestamp)
              .map { _ =>
                result.foreach { case (player, changes) =>
                  replicateTo ! (player -> changes)
                }
                resultPlayers -> result.values.flatten.toSeq
              }
          }
          .flatMap { case (_, mapped) =>
            historyDaoService.insertAll(mapped)
          }
          .recover {
            case ex =>
              ctx.log.error("Exception during updating player stats", ex)
          }
          .foreach { _ =>
            ctx.self ! ClearInProgress(playerIdsSet)
          }

        commandHandler(
          state.copy(
            playersStoringInProgress = state.playersStoringInProgress ++ playerIdsSet
          ),
          schedulerTyped
        )

      case ClearInProgress(fetchedPlayerIds) =>
        val stillWaitingFor = state.playersRequestInProgress.diff(fetchedPlayerIds)

        val playersRequestAwaitsSize = state.playersRequestAwaits.size
        val playersRequestAwaitsText = if (playersRequestAwaitsSize > 0) {
          s"sending to update from queue $playersRequestAwaitsSize players"
        } else {
          "incoming queue is empty"
        }

        val stillWaitingForSize = stillWaitingFor.size
        val stillWaitingText = if(stillWaitingForSize > 0) {
          s"still waiting response for $stillWaitingForSize players"
        } else {
          "ongoing queue is empty"
        }

        ctx.log.info(
          s"Receiving results for {} players, {}, {}",
          fetchedPlayerIds.size,
          playersRequestAwaitsText,
          stillWaitingText,
        )

        ctx.self ! Update(state.playersRequestAwaits.toSeq)

        commandHandler(
          state.copy(
            playersStoringInProgress = state.playersStoringInProgress.diff(fetchedPlayerIds),
            playersRequestInProgress = stillWaitingFor,
            playersRequestAwaits = Set.empty
          ),
          schedulerTyped
        )
    }
  })

}

object PlayerTracker {

  private[behavior] val IndexDiffRegular = 0
  private[behavior] val IndexDiffInconsistent = 1

  private[behavior] object TimerKey

  sealed trait Command
  private[behavior] object Update extends Command
  private[behavior] final case class ValidatedUpdate(notSkip: Boolean) extends Command
  private[behavior] final case class Update(players: Seq[Player]) extends Command
  private[behavior] final case class UpdatePlayerStats(players: Map[Player, Iterable[(String, Double)]]) extends Command
  private[behavior] final case class ClearInProgress(fetchedPlayerIds: Set[ObjectId]) extends Command

}
