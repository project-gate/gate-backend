package me.seroperson.gate.forhonor.behavior

import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials, RawHeader}
import akka.stream.ActorMaterializer
import cats.implicits._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Decoder
import io.circe.generic.JsonCodec
import io.circe.generic.auto._
import me.seroperson.gate.forhonor.behavior.RequestMaker._
import me.seroperson.gate.forhonor.behavior.model.{Shared, SharedRequest}
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model._
import me.seroperson.gate.forhonor.util.ConfigInstance.UbiServices
import org.mongodb.scala.bson.ObjectId
import shapeless.tag.@@

import scala.collection.Seq
import scala.concurrent.ExecutionContext

class RequestMaker(
  executionContext: ExecutionContext @@ Tags.HttpClient,
  materializer: ActorMaterializer @@ Tags.HttpClient
) extends Shared with SharedRequest with FailFastCirceSupport {

  implicit val ec = executionContext
  implicit val mat = materializer

  @JsonCodec
  case class ConnectResponse(
    ticket: String,
    sessionId: String
  )

  @JsonCodec
  case class StatisticsResponse(
    results: Map[String, Map[String, Double]]
  )

  @JsonCodec
  case class ProfileResponse(
    profileId: String,
    nameOnPlatform: String
  )

  implicit final val decodeString: Decoder[String] = Decoder.decodeString.map(_.intern())

  implicit val decodeProfileResponse = Decoder[List[ProfileResponse]].prepare(
    _.downField("profiles")
  )

  implicit val decodeLeaderboardResponse = Decoder[List[String]].prepare(
    _.downField("leaderboard").downField("1")
  )

  /*
  {
    "message": "Ticket is expired",
    "errorCode": 3,
    "httpCode": 401,
    "errorContext": "Profiles Client",
    "moreInfo": "",
    "transactionTime": "2019-03-16T14:13:30.2393681Z",
    "transactionId": "4f314d29-358e-488f-93c6-54eabc018704",
    "environment": "PROD"
  }
  */
  /*
  {
    "serverUtcTime": "2019-03-17 12:13:32",
    "resource": "/public/playerstats2/v1/statistics",
    "httpCode": 401,
    "errorCode": "RendezVous::125",
    "message": "Stripped in prod",
    "moreInfo": ""
  }
  */
  @JsonCodec
  case class ErrorResponse(
    message: String,
    httpCode: Int
  ) extends Error


  def behavior: Behavior[Command] = Behaviors.setup { ctx =>
    val queue = buildUbiPool(ctx.system.toClassic, UbiServices.getString("services-domain"))
    waitingForConnect(queue, List.empty)
  }

  private def buildUbiPool(
    system: ActorSystem,
    domain: String,
    connection: Option[ConnectResponse] = None
  )(implicit materializer: ActorMaterializer) = {
    buildPool(system, domain, { case (request, response) =>
      (request.mapHeaders { headers =>
        (headers :+ (RawHeader("Ubi-AppId", UbiServices.getString("application-id")))) ++ (connection match {
          case Some(x) =>
            Seq(
              RawHeader("Authorization", s"ubi_v1 t=${x.ticket}"),
              RawHeader("Ubi-SessionId", x.sessionId)
            )
          case None => Seq.empty
        })
      }, response)
    })
  }

  private def waitingForConnect(
    connectPool: RequestQueue,
    commandQueue: List[Command]
  ): Behavior[Command] = Behaviors.receive { (ctx, msg) =>
    implicit val pool = connectPool

    def enqueue(command: Command) =
      waitingForConnect(connectPool, commandQueue :+ command)

    msg match {
      case RequestAuthorization =>
        ctx.log.info("Requesting authorization")

        makeRequest[ErrorResponse, ConnectResponse](
          HttpRequest(
            HttpMethods.POST,
            s"/v3/profiles/sessions",
            scala.collection.immutable.Seq(
              Authorization(
                BasicHttpCredentials(
                  UbiServices.getString("user-login"),
                  UbiServices.getString("user-password")
                )
              )
            ),
            HttpEntity.empty(ContentTypes.`application/json`)
          )
        )
          .bimap(
            { error =>
              ctx.log.error("Unable to complete authorization request: {}", error)
              ctx.self ! RequestAuthorizationAfterError
            },
            { connect =>
              ctx.self ! Connected(connect.ticket, connect.sessionId)
            }
          )

        // when receiving `RequestAuthorization` -> changing behavior to the next one
        // tl:dr enqueuing all commands expect of authorization results (`Connected`)
        waitingForConnectionResult(
          connectPool,
          commandQueue
        )

      case ResetAuthorization =>
        Behaviors.same

      // the very first command must be `RequestAuthorization`
      case x =>
        enqueue(x)
    }
  }

  private def waitingForConnectionResult(
    connectPool: RequestQueue,
    commandQueue: List[Command]
  ): Behavior[Command] = Behaviors.receive { (ctx, msg) =>
    implicit val pool = connectPool

    def enqueue(command: Command) =
      waitingForConnectionResult(connectPool, commandQueue :+ command)

    msg match {
      case Connected(ticket, sessionId) =>
        ctx.log.info("Connected successfully")

        commandQueue.foreach {
          ctx.self ! _
        }

        pool.complete()

        val connectResponse = ConnectResponse(ticket, sessionId)
        connected(
          buildUbiPool(
            ctx.system.toClassic,
            UbiServices.getString("services-domain"),
            Some(connectResponse)
          ),
          connectResponse
        )

      case RequestAuthorization =>
        ctx.log.info("Ignoring authorization request as long as it was already requested")

        Behaviors.same

      case RequestAuthorizationAfterError =>
        ctx.log.info("Requesting authorization again after error")
        ctx.self ! RequestAuthorization
        pool.complete()

        // todo preserve queue?
        behavior

      case ResetAuthorization =>
        Behaviors.same

      case x =>
        ctx.log.info("Enqueueing {} message while authorization process is in progress", msg2str(x))

        enqueue(x)
    }
  }

  private def connected(
    requestPool: RequestQueue,
    connection: ConnectResponse
  ): Behavior[Command] = Behaviors.receive { (ctx, msg) =>
    implicit val pool = requestPool

    def handleError = PartialFunction.fromFunction[Error, Error] {
      case error @ ErrorResponse(_, 401) =>
        ctx.log.info("Ticket expired during handling {} message", msg2str(msg))

        ctx.self ! ResetAuthorization
        ctx.self ! RequestAuthorization

        error

      case error @ InternalError(x) =>
        ctx.log.error("Internal error during handling {} message: {}", msg2str(msg), error)
        HandledError(error)

      case error =>
        ctx.log.error("Unknown error during handling {} message: {}", msg2str(msg), error)
        HandledError(error)
    }

    msg match {
      case Statistics(platform, players, replyTo) =>
        /*ctx.log.info(
          "Requesting statistics for {}",
          if(players.size > 5) {
            s"${players.size} players"
          } else {
            players.map(_.name).mkString(",")
          }
        )*/

        makeRequest[ErrorResponse, StatisticsResponse](
          {
            val request = HttpRequest(
              HttpMethods.GET,
              s"/v1/spaces/${platform.spaceId}/sandboxes/${platform.sandboxId}/playerstats2/statistics",
              scala.collection.immutable.Seq.empty,
              HttpEntity.empty(ContentTypes.`application/json`)
            )
            request.withUri(
              request.uri.withQuery(
                Query(
                  "populations" -> players.map(_.profile.id).mkString(","),
                  // GameCompletedCount_Result
                  // GameCompletedCount_Attacker
                  // AssistCount
                  // DeathCount
                  // KillCount_Type
                  // FlagInteractionCount
                  // ObjPointsCount
                  // Level_Hero
                  // HonorableKillCount
                  // TimePlayed_Hero
                  // TimePlayed_Total
                  // TimePlayed_Hero_Rank
                  // TimePlayed_RemotePlay
                  "statistics" -> Stat.values.map(_.name).mkString(",")
                )
              )
            )
          }
        )
          .map { response =>
            players
              .map { incomingPlayer =>
                val profileId = incomingPlayer.profile.id
                val incomingStats = response.results
                  .get(profileId)
                  .filter(_.nonEmpty)
                /*if(incomingStats.isEmpty) {
                  ctx.log.warn("{} has no any stats", profileId)
                }*/
                (
                  incomingPlayer,
                  incomingStats.getOrElse(Map.empty)
                )
              }
              .toMap
          }
          .leftMap(handleError)
          .bimap(
            {
              /* case HandledError(nested) =>
                nested
              case x =>
                ctx.self ! Statistics(platform, players, replyTo)
                x */
              error =>
                // replying with no results if there is any error
                replyTo ! players
                  .map {
                    _ -> Map.empty[String, Double]
                  }
                  .toMap
                error
            },
            {
              replyTo ! _
            }
          )

        Behaviors.same

      case ProfileIdsByNames(platform, names, source, replyTo) =>
        ctx.log.info(
          "Requesting profiles via name for {}",
          if(names.size > 5) {
            s"${names.size} players"
          } else {
            names.mkString("[", ", ", "]")
          }
        )

        makeProfileIdsRequest(platform, replyTo, handleError, source, namesO = Some(names))

        Behaviors.same

      case ProfileIdsByIds(platform, profileIds, source, replyTo) =>
        ctx.log.info(
          "Requesting profiles via profileId for {}",
          if(profileIds.size > 5) {
            s"${profileIds.size} players"
          } else {
            profileIds.mkString("[", ", ", "]")
          }
        )

        makeProfileIdsRequest(platform, replyTo, handleError, source, profileIdsO = Some(profileIds))

        Behaviors.same

      case Leaderboard(platform, replyTo) =>
        ctx.log.info("Requesting leaderboard")

        makeRequest[ErrorResponse, List[String]](
          HttpRequest(
            HttpMethods.GET,
            s"/v1/spaces/${platform.spaceId}/sandboxes/${platform.sandboxId}/game2web/ranking/leaderboard",
            scala.collection.immutable.Seq.empty,
            HttpEntity.empty(ContentTypes.`application/json`)
          )
        )
          .leftMap(handleError)
          .bimap(
            {
              case HandledError(nested) =>
                nested
              case x =>
                ctx.self ! Leaderboard(platform, replyTo)
                x
            },
            {
              replyTo ! _
            }
          )

        Behaviors.same

      case ResetAuthorization =>
        ctx.log.info("Resetting authorization")
        pool.complete()

        behavior
    }
  }

  private def makeProfileIdsRequest(
    platform: Platform,
    replyTo: ActorRef[Set[Player]],
    handleError: Function1[Error, Error],
    source: Int,
    profileIdsO: Option[Seq[String]] = None,
    namesO: Option[Seq[String]] = None
  )(implicit
    ec: ExecutionContext,
    pool: RequestQueue
  ) = {
    makeRequest[ErrorResponse, List[ProfileResponse]](
      {
        val request = HttpRequest(
          HttpMethods.GET,
          "/v3/profiles",
          scala.collection.immutable.Seq.empty,
          HttpEntity.empty(ContentTypes.`application/json`)
        )
        request.withUri(
          request.uri.withQuery(
            profileIdsO match {
              case Some(profileIds) =>
                Query(
                  "profileIds" -> profileIds.mkString(",")
                )
              case None =>
                namesO
                  .map { names =>
                    Query(
                      "platformType" -> platform.id,
                      "nameOnPlatform" -> names.mkString(",")
                    )
                  }
                  .getOrElse(throw new IllegalArgumentException("Profile IDs or names must be passed"))
            }
          )
        )
      })
      .map { profiles =>
        profiles.map { profile =>
          Player(
            new ObjectId(),
            profile.nameOnPlatform,
            Profile(profile.profileId),
            Activity(
              None,
              None,
              None,
              0
            ),
            source,
            List(
              profile.nameOnPlatform
            )
          )
        }
      }
      .leftMap(handleError)
      .bimap(
        {
          case HandledError(nested) =>
            replyTo ! Set.empty
          case _ =>
            replyTo ! Set.empty
            // todo here we need to try again
            // ctx.self ! ProfileIds(players, replyTo)
        },
        { serverPlayers =>
          replyTo ! serverPlayers.toSet
        }
      )
  }

  private def msg2str(msg: RequestMaker.Command) = msg.getClass.getSimpleName
}

object RequestMaker {

  sealed trait Command

  object RequestAuthorization extends Command
  private[behavior] object RequestAuthorizationAfterError extends Command
  private[behavior] object ResetAuthorization extends Command
  private[behavior] final case class Connected(ticket: String, sessionId: String) extends Command

  final case class Statistics(
    platform: Platform,
    players: Seq[Player],
    replyTo: ActorRef[Map[Player, Iterable[(String, Double)]]]
  ) extends Command

  // profileIds.size limited to 50
  final case class ProfileIdsByIds(
    platform: Platform,
    profileIds: Seq[String],
    source: Int,
    replyTo: ActorRef[Set[Player]]
  ) extends Command

  // names.size limited to 50
  final case class ProfileIdsByNames(
    platform: Platform,
    names: Seq[String],
    source: Int,
    replyTo: ActorRef[Set[Player]]
  ) extends Command

  final case class Leaderboard(
    platform: Platform,
    replyTo: ActorRef[List[String]]
  ) extends Command

  // {
  //   "ranking": {
  //     "1": {"tier": 5, "division": 3, "rank": 947, "progression": 4.900563052102692},
  //     "2": {"tier": 0, "progression": 0.0},
  //     "3": {"tier": 0, "progression": 0.0},
  //     "4": {"tier": 0, "progression": 0.0},
  //     "6": {"tier": 0, "progression": 0.0}
  //   }
  // }

}
