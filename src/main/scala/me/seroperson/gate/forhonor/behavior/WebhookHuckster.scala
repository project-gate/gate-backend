package me.seroperson.gate.forhonor.behavior

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import cats.data.EitherT
import cats.implicits._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.JsonCodec
import io.circe.generic.auto._
import me.seroperson.gate.forhonor.behavior.WebhookHuckster._
import me.seroperson.gate.forhonor.behavior.model.{InformationChangeParsed, SharedRequest}
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed._
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.util.ConfigInstance
import shapeless.tag.@@

import scala.jdk.CollectionConverters._
import scala.concurrent.Future

class WebhookHuckster(
  materializer: ActorMaterializer @@ Tags.Behaviour
) extends SharedRequest with FailFastCirceSupport {

  implicit val mat = materializer

  @JsonCodec
  case class WebhookRequest(
    content: String
  )

  @JsonCodec
  case class WebhookResponse(
  )

  @JsonCodec
  case class ErrorResponse(
    code: Int,
    message: String
  ) extends Error

  def behavior: Behavior[Command] = Behaviors.setup { ctx =>
    implicit val queue = buildPool(ctx.system.toClassic, ConfigInstance.Webhook.Discord.getString("domain"))

    val allTargets = ConfigInstance.Webhook.Discord
      .getConfigList("targets")
      .asScala
      .map { element =>
        val url = element.getString("url")
        val sendGameCompleted = if(element.hasPath("send-game-completed")) {
          Some(element.getConfig("send-game-completed"))
        } else {
          None
        }
        (url, sendGameCompleted)
      }

    Behaviors.receive { (ctx, msg) =>
      implicit val executionContext = ctx.system.executionContext

      msg match {
        case ReceiveChanges(name, profileId, changes) =>
          val gameCompletedChanges = changes.filter(_.stat.isGameCompletedCountResult)
          val killCount = {
            val assistCountStats = changes.filter { change =>
              change.stat.isAssistCount &&
                (change.assistant.isHuman ||
                change.assistant.isBot)
            }
            val killCountStats = changes.filter { change =>
              change.stat.isKillCountType &&
                (change.assistant.isHuman ||
                change.assistant.isBot)
            }
            (assistCountStats ++ killCountStats).map(_.value).sum
          }
          val deathCount = changes.filter(_.stat.isDeathCount).map(_.value).sum
          val isSingleGameChanges = changes.isSingleGame
          allTargets
            .filter { case(url, sendGameCompleted) =>
               isSingleGameChanges &&
                 sendGameCompleted.isDefined
            }
            .foreach { case(url, sendGameCompleted) =>
              ctx.log.info("Posting webhook for {}", name)

              val gameResult = gameCompletedChanges.head
              val gameCompletedSettings = sendGameCompleted.get
              val killCountText = if(killCount > 0) {
                gameCompletedSettings.getString("kills-gt-0-format").format(killCount)
              } else {
                gameCompletedSettings.getString("kills-eq-0-format")
              }
              val deathCountText = if(deathCount > 0) {
                gameCompletedSettings.getString("deaths-gt-0-format").format(deathCount)
              } else {
                gameCompletedSettings.getString("deaths-eq-0-format")
              }
              val resultText = if(gameResult.assistant.isUno) {
                gameCompletedSettings.getString("result-win-format")
              } else if(gameResult.assistant.isZero) {
                gameCompletedSettings.getString("result-loose-format")
              } else {
                gameCompletedSettings.getString("result-tie-format")
              }
              val message = gameCompletedSettings.getString("message-format")
                .format(
                  killCountText,
                  deathCountText,
                  resultText,
                  gameResult.game,
                  name,
                  gameResult.hero
                )

              val errorOrEntity = Marshal(WebhookRequest(message))
                .to[RequestEntity]
                .map[Either[Error, RequestEntity]] { entity =>
                  Right(entity)
                }
                .recover {
                  case ex =>
                    Left(InternalError(ex))
                }
              EitherT[Future, Error, RequestEntity](errorOrEntity)
                .flatMap { entity =>
                  makeRequest[ErrorResponse, WebhookResponse](
                    {
                      HttpRequest(
                        HttpMethods.POST,
                        url,
                        Nil,
                        entity
                      )
                    },
                    WebhookResponse()
                  )
                }
                .leftMap {
                  case InternalError(ex) =>
                    ex.printStackTrace()
                  case x =>
                    ctx.log.error("Unknown error: {}", x)
                }
            }
      }

      Behaviors.same
    }
  }

}

object WebhookHuckster {

  sealed trait Command
  final case class ReceiveChanges(name: String, profileId: String, changes: Seq[InformationChangeParsed]) extends Command

}
