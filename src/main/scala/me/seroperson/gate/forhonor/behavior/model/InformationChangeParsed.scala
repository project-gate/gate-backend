package me.seroperson.gate.forhonor.behavior.model

import me.seroperson.gate.forhonor.Main
import me.seroperson.gate.forhonor.model.Game._
import me.seroperson.gate.forhonor.model.Stat._
import me.seroperson.gate.forhonor.model.Assistant._
import me.seroperson.gate.forhonor.model.Hero._
import me.seroperson.gate.forhonor.model.Qualifier._
import me.seroperson.gate.forhonor.model._
import org.mongodb.scala.bson.ObjectId

import scala.concurrent.ExecutionContext

case class InformationChangeParsed(
  playerId: ObjectId,
  timestamp: Long,
  stat: Stat,
  hero: Hero,
  game: Game,
  assistant: Assistant,
  qualifier: Qualifier,
  value: Double,
  isFirstFetch: Boolean
)

object InformationChangeParsed {

  val pvpPredicate: (InformationChangeParsed) => Boolean =
    change => change.qualifier.isPvp

  val humanKillPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isKillCountType && change.assistant.isHuman

  val pvpHumanKillPredicate: (InformationChangeParsed) => Boolean =
    change => humanKillPredicate(change) && pvpPredicate(change)

  val humanKillAssistPredicate: (InformationChangeParsed) => Boolean =
    change => (change.stat.isKillCountType || change.stat.isAssistCount) && change.assistant.isHuman

  val humanAssistPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isAssistCount && change.assistant.isHuman

  val botKillPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isKillCountType && change.assistant.isBot

  val botKillAssistPredicate: (InformationChangeParsed) => Boolean =
    change => (change.stat.isKillCountType || change.stat.isAssistCount) && change.assistant.isBot

  val minionKillPredicate: (InformationChangeParsed) => Boolean =
    change => (change.stat.isKillCountType) && (change.assistant.isMinion || change.assistant.isExtraMinion)

  val humanDeathPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isDeathCount && change.assistant.isHuman

  val pvpHumanDeathPredicate: (InformationChangeParsed) => Boolean =
    change => humanDeathPredicate(change) && pvpPredicate(change)

  val deathPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isDeathCount

  val winCountPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isGameCompletedCountResult && change.assistant.isUno

  val looseCountPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isGameCompletedCountResult && change.assistant.isZero

  val gameCountPredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isGameCompletedCountResult

  val pvpGameCountPredicate: (InformationChangeParsed) => Boolean =
    change => gameCountPredicate.apply(change) && pvpPredicate(change)

  val rankedGamePredicate: (InformationChangeParsed) => Boolean =
    change => change.stat.isGameCompletedCountResult && change.game.isRankedDuel

  def findKeysByPredicate(predicate: (Stat, Hero, Game, Assistant, Qualifier) => Boolean)(implicit ec: ExecutionContext) =
    Main.informationKeyMapping.map { information =>
      information.values
        .toList
        .sortBy { key =>
          parseKey(key)
            .map(_._2.toString())
        }
        .flatMap { key =>
          parseKey(key)
            .map { parsedKeyO =>
              predicate.tupled(parsedKeyO)
            }
            .filter(identity)
            .map { _ =>
              key.hashCode
            }
        }
    }

  def parseKey(key: String) = {
    implicit class ExtendedKey[T](o: Option[T]) {
      def logIfNone(name: String, arg1: Any, arg2: Any, arg3: Any) = o match {
        case None =>
          // ctx.log.warning("Can't parse {} {} with value {} for profile {}", name, arg1, arg2, arg3)
          None
        case some => some
      }
    }
    // DeathCount:B:Hero_ChineseDLC4General:DL:CG:infinite
    // DeathCount:XP:Hero_ChineseDLC4General:DDL:?:infinite
    // TimePlayed_Hero:Hero_VikingHybrid:DDL:PVP:infinite 22245.984882258297 (seconds),
    val statNameGroupId = 1
    val assistantGroupId = 2
    val heroGroupId = 3
    val gameGroupId = 4
    val qualifierGroupId = 5
    val keyRegex5 = """(\w+):([A-Z0-9]+):(\w+):(\w+):([A-Z?]+):infinite""".r
    val keyRegex4 = """(\w+):(\w+):(\w+):([A-Z?]+):infinite""".r
    val dotCount = key.count(_ == ':')
    (if (dotCount == 4) {
      keyRegex4
    } else {
      keyRegex5
    }).findFirstMatchIn(key)
      .flatMap { m =>
        val statApiKey = m.group(statNameGroupId)
        val assistantApiKey = m.group(assistantGroupId)
        val heroApiKey = m.group(heroGroupId)
        val gameApiKey = m.group(gameGroupId)
        val qualifierApiKey = m.group(qualifierGroupId)
        for {
          stat <- Stat.values.find(x => stat2apiKey(x) == statApiKey)
          assistant <- Assistant.values.find(x => assistant2apiKey(x) == assistantApiKey)
          hero <- Hero.values.find(x => hero2apiKey(x) == heroApiKey)
          game <- Game.values.find(x => game2apiKey(x).contains(gameApiKey))
          qualifier <- Qualifier.values.find(x => qualifier2apiKey(x) == qualifierApiKey)
        } yield {
          (
            stat,
            hero,
            game,
            assistant,
            qualifier
          )
        }
      }
  }

  implicit class ExtendedInformationChangeParsedIterable(changes: Iterable[InformationChangeParsed]) {
    def hasWin() = {
      changes
        .exists { change =>
          change.stat.isGameCompletedCountResult && change.assistant.isUno
        }
    }
    def isSingleGame() = {
      val gameCompletedChanges = changes.filter(_.stat.isGameCompletedCountResult)
      gameCompletedChanges.size == 1 &&
        gameCompletedChanges.head.value == 1
    }
  }

  implicit class ExtendedInformationChange(change: InformationChange) {
    def toParsed()(implicit executionContext: ExecutionContext) = {
      Main.informationKeyMapping.map(_.get(change.key).flatMap { key =>
        parseKey(key).map { case (stat, hero, game, assistant, qualifier) =>
          InformationChangeParsed(
            change.playerId,
            change.timestamp,
            stat,
            hero,
            game,
            assistant,
            qualifier,
            change.value,
            change.isFirstFetch
          )
        }
      })
    }
  }

  private def assistant2apiKey(assistant: Assistant) = assistant match {
    case Human => "P"
    case Bot => "B"
    case Captain => "C"
    case Minion => "M"
    case General => "M"
    case Dummy => "D"
    case me.seroperson.gate.forhonor.model.Assistant.Object => "O"
    case Commander => "K"
    case Guardian => "XP"
    case ExtraMinion => "X"
    case Zero => "0"
    case Uno => "1"
    case Two => "2"
  }

  private def hero2apiKey(hero: Hero) = "Hero_" + (hero match {
    case HeroUnspecified => "Unknown"

    case Warden => "KnightChampion"
    case BlackPrior => "KnightH023Darkwarden"
    case PeaceKeeper => "KnightAssassin"
    case Centurion => "KnightDLC1Centurion"
    case Gladiator => "KnightDLC2Gladiator"
    case Lawbringer => "KnightHybrid"
    case Conqueror => "KnightTank"
    case Warmonger => "KnightH027Warmonger"
    case Griphon => "KnightH028Gryphon"

    case Orochi => "SamuraiAssassin"
    case Kensei => "SamuraiChampion"
    case Nobushi => "SamuraiHybrid"
    case Shinobi => "SamuraiDLC1Ninja"
    case Aramusha => "SamuraiDLC3Ronin"
    case Shugoki => "SamuraiTank"
    case Sakura => "SamuraiH024Manslayer"

    case Berserker => "VikingAssassin"
    case Raider => "VikingChampion"
    case Highlander => "VikingDLC2Highlander"
    case Shaman => "VikingDLC3Huntress"
    case Valkyrie => "VikingHybrid"
    case Warlord => "VikingTank"
    case Jormungandr => "VikingH025Zealot"

    case Tiandi => "ChineseDLC4General"
    case Shaolin => "ChineseDLC4Shaolin"
    case Nuxia => "ChineseDLC5Bodyguard"
    case JJ => "ChineseDLC5OldMaster"
    case Zhanhu => "ChineseH026Betrayer"
  })

  // DMN_EDL_HRD: "ENDLESS_DOM_BASIC_HORDE",
  // DMN_EDL_BLD: "ENDLESS_TDM_BASIC_BLEEDING",
  // DMN_CHMP: "DOMINION_CHAMPION_ONLY",
  // DMN_BMM_T1: "DOMINION_BMM_Test001",
  // also idk what is _SMS postfix
  private def game2apiKey(game: Game) = game match {
    case GameUnspecified => List.empty
    case Dominion => List(
      "DMN",
      // ranked
      "R_DMN",
      // season 09? see `the_unima`
      "DMN_S09",
      // halloween event, s11
      "DMN_S11",
      "DMN_S13TU2_W1",
      "DMN_S13TU2_W2",
      // events?
      "DMN_AC",
      "DMN_LD",
      "DMN_FRE",
      "DMN_NR",
      // ?
      "SEDMN"
    )
    case Brawl => List(
      "DDL",
      "DDL_SMS",
      // ?
      "DDL_NR",
      // seems like event too
      "DDL_ICE"
    )
    case Duel => List(
      "DL",
      // ?
      "TU_DL",
      // ?
      "DL_S10"
    )
    case RankedDuel => List(
      "R_DL",
      // tournament?
      "T_DL"
    )
    // smertelnaya shvatka
    case TeamDeathmatch => List(
      "TDM",
      "TDM_S10",

      "TDM_S15TU1_A2",
      "TDM_S15TU1_A3",
      "TDM_S15TU2_A2",
      "TDM_S15TU2_A3"
    )
    // unichtozhenie
    case TeamLastManStanding => List(
      "TLMS",
      "TLMS_SMS",
      // wtf is it?
      "PTLMS_1",
      "PTLMS_2",
      "PTLMS_3",
      "TLMS_S13TU1_W1",
      "TLMS_S13TU1_W2",
      // Metal trials
      "PTLMS_S14TU1_Ch1",
      "PTLMS_S14TU1_Ch2",
      "PTLMS_S14TU1_Ch3",
      // ?
      "SETLMS"
    )
    case Breach => List(
      "TSGM",
      "TSGM_HYPER",
      // ragnarok
      "TSGM_S11",
      "TSGM_S12",

      "TSGM_S15TU1_A1",
      "TSGM_S15TU2_A1"
    )
    // dary
    case Tribute => List(
      "CTF",
      // ?
      "SECTF",
      // ranked
      "R_CTF"
    )
    case Kampaign => List(
      "CPG"
    )
    case Arcade => List(
      "ARC"
    )
    case Training => List(
      "TRL",
      "TRN"
    )
  }

  // ?: ?
  private def qualifier2apiKey(qualifier: Qualifier) = qualifier match {
    case Pvp => "PVP"
    case Pve => "PVE"
    case PrivateMatch => "PM"
    case CustomGame => "CG"
    case Practice => "PRAC"
    case Qualifier.Unknown => "?"
  }

  private def stat2apiKey(stat: Stat) = stat match {
    case DeathCount => "DeathCount"
    case KillCount_Type => "KillCount_Type"
    case GameCompletedCount_Result => "GameCompletedCount_Result"
    case AssistCount => "AssistCount"
    // case TimePlayed_Hero => "TimePlayed_Hero"
  }

}
