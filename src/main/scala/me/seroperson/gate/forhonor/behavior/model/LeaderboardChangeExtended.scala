package me.seroperson.gate.forhonor.behavior.model

import android.support.v7.util.{DiffUtil, ListUpdateCallback}
import me.seroperson.gate.forhonor.model.{Inserted, LeaderboardChange, Moved, Removed}
import org.mongodb.scala.bson.ObjectId

import scala.collection.mutable

object LeaderboardChangeExtended {

  implicit class ListExtended(initial: List[String]) {

    def diffWith(updated: List[String]): Seq[LeaderboardChange] = {
      val result = mutable.ArrayDeque[LeaderboardChange]()
      val timestamp = System.currentTimeMillis()
      DiffUtil.calculateDiff(new DiffUtil.Callback() {
        override def getOldListSize = initial.size
        override def getNewListSize = updated.size
        override def areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
          initial(oldItemPosition) == updated(newItemPosition)
        override def areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
          areItemsTheSame(oldItemPosition, newItemPosition)
        override def getItemOnPosition(itemPositionInAfter: Int) =
          updated(itemPositionInAfter)
      }).dispatchUpdatesTo(new ListUpdateCallback {
        override def onInserted(position: Int, item: Any) {
          result += LeaderboardChange(
            new ObjectId(),
            timestamp,
            Inserted(
              item.asInstanceOf[String],
              position
            )
          )
        }
        override def onRemoved(position: Int) {
          result += LeaderboardChange(
            new ObjectId(),
            timestamp,
            Removed(
              position
            )
          )
        }
        override def onMoved(fromPosition: Int, toPosition: Int) {
          result += LeaderboardChange(
            new ObjectId(),
            timestamp,
            Moved(
              fromPosition,
              toPosition
            )
          )
        }
      })
      result.toSeq
    }

    def applyDiff(diff: Seq[LeaderboardChange]): List[String] = {
      import scala.jdk.CollectionConverters._

      val current = new java.util.ArrayList[String](initial.asJava)

      diff
        .foldLeft(current) { case (result, change) =>
          change.changeType match {
            case Removed(from) =>
              result.remove(from)
            case Inserted(profileId, to) =>
              result.add(to, profileId)
            case Moved(from, to) =>
              val removed = result.remove(from)
              result.add(to, removed)
          }
          result
        }
        .asScala
        .toList
    }

  }

}
