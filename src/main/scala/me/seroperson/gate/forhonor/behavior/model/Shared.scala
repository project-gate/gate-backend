package me.seroperson.gate.forhonor.behavior.model

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.scaladsl.{Keep, Sink, Source, SourceQueueWithComplete}
import akka.stream.{ActorMaterializer, OverflowStrategy, QueueOfferResult}
import akka.util.ByteString
import cats.data.EitherT

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success}

trait Shared

trait SharedRequest extends SharedError {

  type RequestQueue = SourceQueueWithComplete[(HttpRequest, Promise[HttpResponse])]

  protected def buildPool(
    system: ActorSystem,
    domain: String,
    requestInterceptor: Tuple2[HttpRequest, Promise[HttpResponse]] => (HttpRequest, Promise[HttpResponse]) = identity
  )(implicit materializer: ActorMaterializer) = {
    Source.queue[(HttpRequest, Promise[HttpResponse])](Int.MaxValue, OverflowStrategy.fail)
      .map(requestInterceptor)
      .via(Http.get(system).cachedHostConnectionPoolHttps[Promise[HttpResponse]](domain))
      .toMat(
        Sink.foreach {
          case (Success(resp), p) => p.success(resp)
          case (Failure(e), p) => p.failure(e)
        }
      )(Keep.left)
      .run()
  }

  def queueRequest(request: HttpRequest)(
    implicit pool: RequestQueue,
    executionContext: ExecutionContext
  ) = {
    val responsePromise = Promise[HttpResponse]()
    pool.offer(request -> responsePromise).flatMap {
      case QueueOfferResult.Enqueued => responsePromise.future
      case QueueOfferResult.Failure(ex) => Future.failed(ex)
      case QueueOfferResult.Dropped => Future.failed(new RuntimeException("QueueOfferResult.Dropped"))
      case QueueOfferResult.QueueClosed => Future.failed(new RuntimeException("QueueOfferResult.QueueClosed"))
    }
  }

  def makeRequest[E <: Error, T](
    request: HttpRequest,
    // todo
    ifEmpty: T = null
  )(
    implicit unmarshallerT: Unmarshaller[ResponseEntity, T],
    unmarshallerError: Unmarshaller[ResponseEntity, E],
    pool: RequestQueue,
    executionContext: ExecutionContext,
    materializer: ActorMaterializer
  ): EitherT[Future, Error, T] = {
    implicit class ExtendedFuture(f: Future[Either[Error, T]]) {
      def recoverWithError = f.recover {
        case ex =>
          Left[Error, T](InternalError(ex))
      }
    }
    EitherT(
      queueRequest(request)
        .flatMap { response =>
          if(response.status.isSuccess()) {
            // todo
            if(response.status == StatusCodes.NoContent) {
              response.discardEntityBytes()
              Future.successful(Right(ifEmpty))
            } else {
              Unmarshal(response.entity)
                .to[T]
                .map[Either[Error, T]](t => Right(t))
                .recoverWithError
            }
          } else {
            Unmarshal(response.entity)
              .to[E]
              .map(error => Left[Error, T](error))
              .recoverWithError
          }
        }
        .recover {
          case x =>
            Left[Error, T](InternalError(x))
        }
    )
  }

}

trait SharedError {

  case class InternalError(
    throwable: Throwable
  ) extends Error

  case class HandledError(
    error: Error
  ) extends Error

  trait Error

}
