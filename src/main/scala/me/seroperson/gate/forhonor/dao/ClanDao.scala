package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.ClanApi
import me.seroperson.gate.forhonor.model.StoredClan
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.{MongoCollection, _}

class ClanDao(val clansCollection: MongoCollection[StoredClan]) extends ClanApi {

  def findAll() = {
    clansCollection
      .find()
      .toFuture()
  }

  def findByNameId(id: String) = {
    clansCollection
      .find(
        equal("name_id", id)
      )
      .headOption()
  }

}
