package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.{ClanApi, PlayerApi}
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.{Player, StoredClan}
import shapeless.tag.@@

import scala.concurrent.{ExecutionContext, Future}

class ClanDaoService(
  clanApi: ClanApi,
  playerApi: PlayerApi,
  executionContext: ExecutionContext @@ Tags.DaoExecutionContext
) {

  implicit val ec = executionContext

  def findAll() = clanApi.findAll()
  def findByNameId(id: String) = clanApi.findByNameId(id)

  /**
    * @return Leader to members
    * */
  def findPlayers(clan: StoredClan): Future[(Option[Player], Seq[Player])] = {
    val playerIds = clan.byPlayerId
    val playerRegexes = clan.byRegex
    val playersByIdF = if(playerIds.nonEmpty) {
      playerApi.findByIds(playerIds)
    } else {
      Future.successful(Seq.empty)
    }
    val playersByRegexF = if(playerRegexes.nonEmpty) {
      playerApi.findByRegexes(playerRegexes)
    } else {
      Future.successful(Seq.empty)
    }
    for {
      playersById <- playersByIdF
      playersByRegex <- playersByRegexF
      leader <- playerApi.findById(clan.leaderPlayerId)
    } yield {
      leader -> (playersById ++ playersByRegex).distinct.diff(leader.toSeq)
    }
  }

}
