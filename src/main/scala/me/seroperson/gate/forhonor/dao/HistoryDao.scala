package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.HistoryApi
import me.seroperson.gate.forhonor.model.{InformationChange, LiveCapturedResponse, TotalInformationResponse}
import org.mongodb.scala.{MongoCollection, _}
import org.mongodb.scala.bson.{BsonArray, BsonDocument, ObjectId}
import org.mongodb.scala.model.Filters._

import scala.concurrent.Future

class HistoryDao(val historyCollection: MongoCollection[InformationChange]) extends HistoryApi {

  def findTotal(playerIds: Seq[ObjectId]) = {
    historyCollection
      .aggregate[TotalInformationResponse](
        Seq(
          BsonDocument(
            "$match" -> BsonDocument(
              "player_id" -> BsonDocument(
                "$in" -> playerIds
              )
            )
          ),
          BsonDocument(
            "$group" -> BsonDocument(
              "_id" -> BsonDocument(
                "player_id" -> "$player_id",
                "key" -> "$key",
              ),
              "value" -> BsonDocument(
                "$sum" -> "$value"
              )
            )
          ),
          BsonDocument(
            "$group" -> BsonDocument(
              "_id" -> "$_id.player_id",
              "keys" -> BsonDocument(
                "$push" -> BsonDocument(
                  "key" -> "$_id.key",
                  "value" -> "$value"
                )
              )
            )
          )
        )
      )
      .toFuture()
  }

  def findLiveCaptured(keys: Seq[Int], playerIds: Seq[ObjectId], from: Long, to: Long): Future[Seq[LiveCapturedResponse]] = {
    findLiveCaptured(
      BsonDocument(
        "$match" -> BsonDocument(
          "player_id" -> BsonDocument(
            "$in" -> playerIds
          ),
          "$and" -> BsonArray(
            BsonDocument(
              "timestamp" -> BsonDocument(
                "$gte" -> from
              )
            ),
            BsonDocument(
              "timestamp" -> BsonDocument(
                "$lte" -> to
              )
            )
          ),
          "key" -> BsonDocument(
            "$in" -> keys
          ),
          "is_first_fetch" -> false
        )
      ),
      pagination = None
    )
  }

  def findPaginatedLiveCaptured(playerIds: Seq[ObjectId], firstTimestamp: Long, skip: Int, limit: Int): Future[Seq[LiveCapturedResponse]] = {
    findLiveCaptured(
      BsonDocument(
        "$match" -> BsonDocument(
          "player_id" -> BsonDocument(
            "$in" -> playerIds
          ),
          "timestamp" -> BsonDocument(
            "$lte" -> firstTimestamp
          ),
          "is_first_fetch" -> false
        )
      ),
      pagination = Some(
        PaginationParams(
          skip,
          limit
        )
      )
    )
  }

  // -- unused --

  def findLiveCaptured(from: Long, to: Long): Future[Seq[LiveCapturedResponse]] = {
    findLiveCaptured(
      BsonDocument(
        "$match" -> BsonDocument(
          "$and" -> BsonArray(
            BsonDocument(
              "timestamp" -> BsonDocument(
                "$gte" -> from
              )
            ),
            BsonDocument(
              "timestamp" -> BsonDocument(
                "$lte" -> to
              )
            )
          ),
          "is_first_fetch" -> false
        )
      ),
      pagination = None
    )
  }

  // -- unused --

  private case class PaginationParams(
    skip: Int,
    limit: Int
  )

  private def findLiveCaptured(
    matchPipeline: BsonDocument,
    pagination: Option[PaginationParams]
  ) = {
    historyCollection
      .aggregate[LiveCapturedResponse](
        Seq
          .apply(
            matchPipeline,
            BsonDocument(
              "$group" -> BsonDocument(
                "_id" -> BsonDocument(
                  "player_id" -> "$player_id",
                  "timestamp" -> "$timestamp"
                ),
                "changes" -> BsonDocument(
                  "$push" -> BsonDocument(
                    "key" -> "$key",
                    "value" -> "$value"
                  )
                )
              )
            )
          )
          .++(
            pagination
              .map { value =>
                Seq(
                  BsonDocument(
                    "$sort" -> BsonDocument(
                      "_id.timestamp" -> -1
                    )
                  ),
                  BsonDocument(
                    "$skip" -> value.skip
                  ),
                  BsonDocument(
                    "$limit" -> value.limit
                  )
                )
              }
              .getOrElse(Seq.empty)
          )
          .:+(
            BsonDocument(
              "$group" -> BsonDocument(
                "_id" -> "$_id.player_id",
                "iterations" -> BsonDocument(
                  "$push" -> BsonDocument(
                    "timestamp" -> "$_id.timestamp",
                    "changes" -> "$changes"
                  )
                )
              )
            )
          )
      )
      .allowDiskUse(true)
      .toFuture()
  }

  def findAll() = {
    historyCollection
      .find()
      .toFuture()
  }

  def findForInconsistency(playerId: ObjectId, value: Seq[(Int, Double)]) = {
    historyCollection
      .find(
        and(
          equal("player_id", playerId),
          or(
            value
              .map { case (key, value) =>
                and(
                  equal("key", key),
                  equal("value", value)
                )
              }:_*
          )
        )
      )
      .toFuture()
  }

  def findByPlayerBefore(id: ObjectId, before: Long) = {
    historyCollection
      .find(
        and(
          equal("player_id", id),
          lte("timestamp", before)
        )
      )
      .toFuture()
  }

  def findByPlayerIntervalIds(idIntervals: Seq[(ObjectId, (Long, Long))]) = {
    historyCollection
      .find(
        or(
          idIntervals.map { case (playerId, (from, to)) =>
            and(
              equal("player_id", playerId),
              gte("timestamp", from),
              lte("timestamp", to)
            )
          }:_*
        )
      )
      .toFuture()
  }

  def findByPlayerIds(ids: Seq[ObjectId], from: Long, to: Long) = {
    historyCollection
      .find(
        and(
          in[ObjectId]("player_id", ids:_*),
          gte("timestamp", from),
          lte("timestamp", to)
        )
      )
      .toFuture()
  }

  def findByPlayerId(id: ObjectId, from: Long, to: Long) = {
    historyCollection
      .find(
        and(
          equal("player_id", id),
          gte("timestamp", from),
          lte("timestamp", to)
        )
      )
      .toFuture()
  }

  def findByPlayerId(id: ObjectId, to: Long) = {
    historyCollection
      .find(
        and(
          equal("player_id", id),
          lte("timestamp", to)
        )
      )
      .toFuture()
  }

  def findByPlayerId(id: ObjectId) = {
    historyCollection
      .find(
        equal("player_id", id)
      )
      .toFuture()
  }

  def findByPlayerIds(ids: Seq[ObjectId]) = {
    historyCollection
      .find(
        in("player_id", ids:_*)
      )
      .toFuture()
  }

  def insert(change: InformationChange) = {
    historyCollection
      .insertOne(change)
      .toFuture()
  }

  def insertAll(changes: Seq[InformationChange]) = {
    historyCollection
      .insertMany(changes)
      .toFuture()
  }

}
