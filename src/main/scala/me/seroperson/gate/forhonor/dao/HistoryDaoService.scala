package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed._
import me.seroperson.gate.forhonor.dao.api.HistoryApi
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.{InformationChange, LiveCapturedResponse, Player}
import org.mongodb.scala.Completed
import org.mongodb.scala.bson.ObjectId
import shapeless.tag.@@

import scala.concurrent.{ExecutionContext, Future}

class HistoryDaoService(
  historyApi: HistoryApi,
  executionContext: ExecutionContext @@ Tags.DaoExecutionContext
) {

  implicit val ec = executionContext

  def findLiveCaptured(
    keys: Seq[Int],
    players: Seq[Player],
    from: Long,
    to: Long
  ): Future[Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] = {
    if(players.isEmpty) {
      Future.successful(Map.empty)
    } else {
      val playerIds = players.map {
        _.id
      }
      historyApi
        .findLiveCaptured(keys, playerIds, from, to)
        .flatMap(findLiveCaptured)
    }
  }

  def findPaginatedLiveCaptured(
    players: Seq[Player],
    firstTimestamp: Long,
    skip: Int,
    limit: Int
  ): Future[Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] = {
    if(players.isEmpty) {
      Future.successful(Map.empty)
    } else {
      val playerIds = players.map {
        _.id
      }
      historyApi
        .findPaginatedLiveCaptured(playerIds, firstTimestamp, skip, limit)
        .flatMap(findLiveCaptured)
    }
  }

  // -- unused --

  def findLiveCaptured(from: Long, to: Long): Future[Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] = {
    historyApi
      .findLiveCaptured(from, to)
      .flatMap(findLiveCaptured)
  }

  // -- unused --

  private def findLiveCaptured(changes: Seq[LiveCapturedResponse]) = {
    Future
      .sequence(
        changes
          .map { byPlayerChanges =>
            Future
              .sequence(
                byPlayerChanges.iterations.map { iteration =>
                  Future
                    .sequence(
                      iteration.changes
                        .map { change =>
                          InformationChange
                            .apply(
                              new ObjectId(),
                              byPlayerChanges.playerId,
                              iteration.timestamp,
                              change.key,
                              change.value,
                              false
                            )
                            .toParsed()
                        }
                    )
                    .map { parsedChanges =>
                      iteration.timestamp -> parsedChanges.flatten
                    }
                }
              )
              .map { iterations =>
                byPlayerChanges.playerId -> iterations.toMap
              }
          }
      )
      .map { handled =>
        handled.toMap
      }
  }

  def findTotal(playerIds: Seq[ObjectId]) =
    historyApi.findTotal(playerIds)
  def findAll() =
    historyApi.findAll()
  def findForInconsistency(playerId: ObjectId, values: Seq[(Int, Double)]) =
    historyApi.findForInconsistency(playerId, values)
  def findByPlayerIntervalIds(idIntervals: Seq[(ObjectId, (Long, Long))]) =
    historyApi.findByPlayerIntervalIds(idIntervals)
  def findByPlayerIds(ids: Seq[ObjectId], from: Long, to: Long) =
    historyApi.findByPlayerIds(ids, from, to)
  def findByPlayerId(id: ObjectId, from: Long, to: Long) =
    historyApi.findByPlayerId(id, from, to)
  def findByPlayerId(id: ObjectId, to: Long) =
    historyApi.findByPlayerId(id, to)
  def findByPlayerId(id: ObjectId) =
    historyApi.findByPlayerId(id)
  def findByPlayerIds(ids: Seq[ObjectId]) = {
    if(ids.isEmpty) {
      Future.successful(Seq.empty)
    } else {
      historyApi.findByPlayerIds(ids)
    }
  }
  def insertAll(changes: Seq[InformationChange]) = {
    if(changes.isEmpty) {
      Future.successful(Completed())
    } else {
      historyApi.insertAll(changes)
    }
  }

}
