package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.InconsistencyApi
import me.seroperson.gate.forhonor.model.Inconsistency
import org.mongodb.scala.{Completed, MongoCollection}
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.model.{UpdateOneModel, UpdateOptions}

class InconsistencyDao(val inconsistencyCollection: MongoCollection[Inconsistency]) extends InconsistencyApi {

  def insertIfIsNotExists(keys: Seq[Inconsistency]) = {
    inconsistencyCollection
      .bulkWrite(
        keys.map { key =>
          val id = key.id
          UpdateOneModel(
            BsonDocument(
              "_id" -> key.id
            ),
            BsonDocument(
              "$set" -> BsonDocument(
                "_id" -> key.id
              )
            ),
            UpdateOptions()
              .upsert(true)
          )
        }
      )
      .toFuture()
  }

}
