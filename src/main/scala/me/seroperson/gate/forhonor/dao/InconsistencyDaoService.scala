package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.InconsistencyApi
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.Inconsistency
import org.bson.types.ObjectId
import shapeless.tag.@@

import scala.concurrent.{ExecutionContext, Future}

class InconsistencyDaoService(
  inconsistencyApi: InconsistencyApi,
  executionContext: ExecutionContext @@ Tags.DaoExecutionContext
) {

  implicit val ec = executionContext

  def insertIfIsNotExists(keys: Seq[ObjectId]) = {
    if(keys.isEmpty) {
      Future.successful(0)
    } else {
      inconsistencyApi.insertIfIsNotExists(keys.map(Inconsistency.apply))
        .map(_.getUpserts.size())
    }
  }

}
