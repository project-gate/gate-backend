package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.LeaderboardApi
import me.seroperson.gate.forhonor.model.LeaderboardChange
import org.mongodb.scala.{MongoCollection, _}
import org.mongodb.scala.model.Filters._

class LeaderboardDao(val leaderboardCollection: MongoCollection[LeaderboardChange]) extends LeaderboardApi {

  def count() = {
    leaderboardCollection
      .countDocuments()
      .toFuture()
  }

  def findAll() = {
    leaderboardCollection
      .find(
        // ascending("timestamp")
      )
      .toFuture()
  }

  def findSince(timestamp: Long) = {
    leaderboardCollection
      .find(
        gte("timestamp", timestamp)
      )
      .toFuture()
  }

  def insert(change: LeaderboardChange) = {
    leaderboardCollection
      .insertOne(change)
      .toFuture()
  }

  def insertAll(changes: Seq[LeaderboardChange]) = {
    leaderboardCollection
      .insertMany(changes)
      .toFuture()
  }

}
