package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.behavior.model.LeaderboardChangeExtended._
import me.seroperson.gate.forhonor.dao.api.LeaderboardApi
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.LeaderboardChange
import org.mongodb.scala.Completed
import shapeless.tag.@@

import scala.concurrent.{ExecutionContext, Future}

class LeaderboardDaoService(
  leaderboardApi: LeaderboardApi,
  executionContext: ExecutionContext @@ Tags.DaoExecutionContext
) {

  implicit val ec = executionContext

  def count() = {
    leaderboardApi.count()
  }

  def composeLeaderboardHistory() = {
    leaderboardApi
      .findAll()
      .map { changes =>
        changes
          .groupBy(_.timestamp)
          .toList
          .sortBy(_._1)
          .foldLeft(List.empty[(Long, List[String])]) { case(currentState, iteration) =>
            val lastState = currentState.lastOption.getOrElse(iteration._1 -> List.empty)
            currentState :+ (iteration._1 -> lastState._2.applyDiff(iteration._2))
          }
      }
  }

  def composeLeaderboard() = {
    leaderboardApi
      .findAll()
      .map { changes =>
        List.empty[String].applyDiff(changes.sortBy(_.id))
      }
  }

  def findAll() = leaderboardApi.findAll()
  def insertAll(changes: Seq[LeaderboardChange]) = {
    if(changes.isEmpty) {
      Future.successful(Completed)
    } else {
      leaderboardApi.insertAll(changes)
    }
  }

}
