package me.seroperson.gate.forhonor.dao

import akka.stream.scaladsl.Source
import me.seroperson.gate.forhonor.dao.api.PlayerApi
import me.seroperson.gate.forhonor.model.Player
import org.bson.BsonNull
import org.mongodb.scala.{MongoCollection, _}
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.bson.{BsonDocument, ObjectId}
import org.mongodb.scala.model.changestream.ChangeStreamDocument
import org.mongodb.scala.model.{UpdateManyModel, UpdateOneModel}
import me.seroperson.gate.forhonor.util.MongoImplicits._

import scala.concurrent.Future

class PlayerDao(val playersCollection: MongoCollection[Player]) extends PlayerApi {

  def observeAll() = {
    val publisher = playersCollection
      .watch(
        Seq(
        )
      )
      .toPublisher()
    Source.fromPublisher(publisher)
      .map { change =>
        Seq(change.getFullDocument)
      }
  }

  def findAll() = {
    playersCollection
      .find()
      .toFuture()
  }

  def findById(id: ObjectId) = {
    playersCollection
      .find(
        equal("_id", id)
      )
      .headOption()
  }

  def findByIds(ids: Seq[ObjectId]) = {
    playersCollection
      .find(
        in[ObjectId]("_id", ids:_*)
      )
      .toFuture()
  }

  def findByName(name: String) = {
    playersCollection
      .find(
        equal("name", name)
      )
      .headOption()
  }

  override def findByRegex(regexValue: String) = {
    playersCollection
      .find(
        regex("name", regexValue)
      )
      .toFuture()
  }

  def findByRegexes(regexes: Seq[String]) = {
    playersCollection
      .find(
        or(
          regexes
            .map { regexValue =>
              regex("name", regexValue)
            }:_*
        )
      )
      .toFuture()
  }

  def findByProfileId(profileId: String) = {
    playersCollection
      .find(
        equal("profile.id", profileId)
      )
      .headOption()
  }

  def findByProfileIds(profileIds: Seq[String]) = {
    playersCollection
      .find(
        in[String]("profile.id", profileIds:_*)
      )
      .toFuture()
  }

  def updatePlayersActivity(changedPlayerIds: Seq[ObjectId], offlinePlayerIds: Seq[ObjectId], timestamp: Long) = {
    playersCollection
      .bulkWrite(
        Seq(
          UpdateManyModel(
            BsonDocument(
              "_id" -> BsonDocument(
                "$in" -> changedPlayerIds
              )
            ),
            BsonDocument(
              "$set" -> BsonDocument(
                "activity.last_activity" -> timestamp,
                "activity.last_activity_check" -> timestamp,
                "activity.inactivity_combo" -> 0
              )
            )
          ),
          UpdateManyModel(
            BsonDocument(
              "_id" -> BsonDocument(
                "$in" -> changedPlayerIds
              ),
              // https://docs.mongodb.com/manual/tutorial/query-for-null-fields/
              "activity.first_activity" -> BsonNull.VALUE
            ),
            BsonDocument(
              "$set" -> BsonDocument(
                "activity.first_activity" -> timestamp
              )
            )
          ),
          UpdateManyModel(
            BsonDocument(
              "_id" -> BsonDocument(
                "$in" -> offlinePlayerIds
              )
            ),
            BsonDocument(
              "$set" -> BsonDocument(
                "activity.last_activity_check" -> timestamp
              ),
              "$inc" -> BsonDocument(
                "activity.inactivity_combo" -> 1
              )
            )
          )
        )
      )
      .map(_ => Completed())
      .toFuture()
  }

  def updateOrInsert(players: Seq[Player]) = {
    playersCollection
      .bulkWrite(
        players
          .map { player =>
            UpdateOneModel(
              BsonDocument(
                "name" -> BsonDocument(
                  "$ne" -> player.name
                ),
                "profile.id" -> player.profile.id
              ),
              BsonDocument(
                "$set" -> BsonDocument(
                  "name" -> player.name
                ),
                "$push" -> BsonDocument(
                  "name_history" -> player.name
                )
              )
            )
          }
          .toSeq
      )
      .map(_ => Completed())
      .toFuture()
  }

  def updateName(stored: Player, given: Player) = {
    playersCollection
      .updateOne(
        BsonDocument(
          "_id" -> stored.id
        ),
        BsonDocument(
          "$set" -> BsonDocument(
            "name" -> given.name
          ),
          "$push" -> BsonDocument(
            "name_history" -> stored.name
          )
        )
      )
      .map(_ => Completed())
      .toFuture()
  }

  def insert(player: Player) = {
    playersCollection
      .insertOne(player)
      .toFuture()
  }

  def insertAll(players: Seq[Player]) = {
    playersCollection
      .insertMany(players)
      .toFuture()
  }

}
