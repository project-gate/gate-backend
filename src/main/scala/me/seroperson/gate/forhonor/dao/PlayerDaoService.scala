package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.PlayerApi
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.Player
import org.mongodb.scala.Completed
import shapeless.tag.@@

import scala.concurrent.{ExecutionContext, Future}

class PlayerDaoService(
  playerApi: PlayerApi,
  executionContext: ExecutionContext @@ Tags.DaoExecutionContext
) {

  implicit val ec = executionContext

  def insertIfNothing(player: Player) = {
    playerApi.findByProfileId(player.profile.id)
      .flatMap { result =>
        result match {
          case Some(stored) =>
            // if there is already such player - updating it with new info (if any)
            if(stored.name != player.name) {
              playerApi.updateName(stored, player)
                .map { _ =>
                  stored.copy(name = player.name)
                }
                .recover { case _ =>
                  // if any error occurred during updating - ignoring it and returning stored player
                  stored
                }
            } else {
              Future.successful(stored)
            }
          case None =>
            // if we unable to find such player - just inserting the given element
            playerApi
              .insert(player)
              .map { _ =>
                // todo return stored player
                player
              }
        }
      }
  }

  def updatePlayersActivity(changedPlayerIds: Seq[Player], offlinePlayerIds: Seq[Player], timestamp: Long) = {
    if(changedPlayerIds.isEmpty && offlinePlayerIds.isEmpty) {
      Future.successful(Completed())
    } else {
      playerApi.updatePlayersActivity(changedPlayerIds.map(_.id), offlinePlayerIds.map(_.id), timestamp)
        .map { _ =>
          Completed()
        }
    }
  }

  def observeAll() = playerApi.observeAll()
  def findAll() = playerApi.findAll()
  def findByName(name: String) = playerApi.findByName(name)
  def findByProfileId(id: String) = playerApi.findByProfileId(id)
  def findByProfileIds(profileIds: Seq[String]) = {
    if(profileIds.isEmpty) {
      Future.successful(Seq.empty)
    } else {
      playerApi.findByProfileIds(profileIds)
    }
  }
  def insertOrUpdate(players: Seq[Player]) = {
    if(players.isEmpty) {
      Future.successful(Completed())
    } else {
      playerApi
        .updateOrInsert(players)
        .map(_ => Completed())
    }
  }
  def insertAll(players: Seq[Player]) = {
    if(players.isEmpty) {
      Future.successful(Completed())
    } else {
      playerApi.insertAll(players)
    }
  }

}
