package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.UnknownKeyApi
import me.seroperson.gate.forhonor.model.UnknownKey
import org.mongodb.scala.{Completed, MongoCollection}
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.model.{UpdateOneModel, UpdateOptions}

class UnknownKeyDao(val unknownKeyCollection: MongoCollection[UnknownKey]) extends UnknownKeyApi {

  def insertIfIsNotExists(keys: Seq[UnknownKey]) = {
    unknownKeyCollection
      .bulkWrite(
        keys.map { key =>
          val id = key.id
          UpdateOneModel(
            BsonDocument(
              "_id" -> id
            ),
            BsonDocument(
              "$set" -> BsonDocument(
                "_id" -> id
              )
            ),
            UpdateOptions().upsert(true)
          )
        }
      )
      .toFuture()
  }

}
