package me.seroperson.gate.forhonor.dao

import me.seroperson.gate.forhonor.dao.api.UnknownKeyApi
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.model.UnknownKey
import shapeless.tag.@@

import scala.concurrent.{ExecutionContext, Future}

class UnknownKeyDaoService(
  unknownKeyApi: UnknownKeyApi,
  executionContext: ExecutionContext @@ Tags.DaoExecutionContext
) {

  implicit val ec = executionContext

  def insertIfIsNotExists(keys: Set[String]) = {
    if(keys.isEmpty) {
      Future.successful(0)
    } else {
      unknownKeyApi.insertIfIsNotExists(keys.map(UnknownKey.apply).toSeq)
        .map(_.getUpserts.size())
    }
  }

}
