package me.seroperson.gate.forhonor.dao.api

import me.seroperson.gate.forhonor.model.StoredClan

import scala.concurrent.Future

trait ClanApi {

  def findAll(): Future[Seq[StoredClan]]

  def findByNameId(id: String): Future[Option[StoredClan]]

}
