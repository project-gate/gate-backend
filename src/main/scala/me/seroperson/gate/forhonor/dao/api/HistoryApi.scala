package me.seroperson.gate.forhonor.dao.api

import me.seroperson.gate.forhonor.model.{InformationChange, LiveCapturedResponse, TotalInformationResponse}
import org.bson.types.ObjectId
import org.mongodb.scala.Completed

import scala.concurrent.Future

trait HistoryApi {

  def findTotal(playerIds: Seq[ObjectId]): Future[Seq[TotalInformationResponse]]

  def findPaginatedLiveCaptured(playerIds: Seq[ObjectId], firstTimestamp: Long, skip: Int, limit: Int): Future[Seq[LiveCapturedResponse]]

  def findLiveCaptured(keys: Seq[Int], playerIds: Seq[ObjectId], from: Long, to: Long): Future[Seq[LiveCapturedResponse]]

  // -- unused --

  def findLiveCaptured(from: Long, to: Long): Future[Seq[LiveCapturedResponse]]

  // -- unused --

  def findAll(): Future[Seq[InformationChange]]

  def findForInconsistency(playerId: ObjectId, value: Seq[(Int, Double)]): Future[Seq[InformationChange]]

  def findByPlayerBefore(id: ObjectId, before: Long): Future[Seq[InformationChange]]

  def findByPlayerIntervalIds(idIntervals: Seq[(ObjectId, (Long, Long))]): Future[Seq[InformationChange]]

  def findByPlayerIds(ids: Seq[ObjectId], from: Long, to: Long): Future[Seq[InformationChange]]

  def findByPlayerId(id: ObjectId, from: Long, to: Long): Future[Seq[InformationChange]]

  def findByPlayerId(id: ObjectId, to: Long): Future[Seq[InformationChange]]

  def findByPlayerId(id: ObjectId): Future[Seq[InformationChange]]

  def findByPlayerIds(ids: Seq[ObjectId]): Future[Seq[InformationChange]]

  def insert(change: InformationChange): Future[Completed]

  def insertAll(changes: Seq[InformationChange]): Future[Completed]

}
