package me.seroperson.gate.forhonor.dao.api

import me.seroperson.gate.forhonor.model.Inconsistency
import org.mongodb.scala.BulkWriteResult

import scala.concurrent.Future

trait InconsistencyApi {

  def insertIfIsNotExists(keys: Seq[Inconsistency]): Future[BulkWriteResult]

}
