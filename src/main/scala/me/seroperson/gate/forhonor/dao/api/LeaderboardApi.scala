package me.seroperson.gate.forhonor.dao.api

import me.seroperson.gate.forhonor.model.LeaderboardChange
import org.mongodb.scala.Completed

import scala.concurrent.Future

trait LeaderboardApi {

  def count(): Future[Long]

  def findAll(): Future[Seq[LeaderboardChange]]

  def findSince(timestamp: Long): Future[Seq[LeaderboardChange]]

  def insert(change: LeaderboardChange): Future[Completed]

  def insertAll(changes: Seq[LeaderboardChange]): Future[Completed]

}
