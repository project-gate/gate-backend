package me.seroperson.gate.forhonor.dao.api

import akka.NotUsed
import akka.stream.scaladsl.Source
import me.seroperson.gate.forhonor.model.Player
import org.bson.types.ObjectId
import org.mongodb.scala.Completed

import scala.concurrent.Future

trait PlayerApi {

  def observeAll(): Source[Seq[Player], NotUsed]

  def findAll(): Future[Seq[Player]]

  def findById(id: ObjectId): Future[Option[Player]]

  def findByIds(ids: Seq[ObjectId]): Future[Seq[Player]]

  def findByName(name: String): Future[Option[Player]]

  def findByRegex(regex: String): Future[Seq[Player]]

  def findByRegexes(regexes: Seq[String]): Future[Seq[Player]]

  def findByProfileId(profileId: String): Future[Option[Player]]

  def findByProfileIds(profileIds: Seq[String]): Future[Seq[Player]]

  def updatePlayersActivity(changedPlayerIds: Seq[ObjectId], offlinePlayerIds: Seq[ObjectId], timestamp: Long): Future[Seq[Completed]]

  def updateOrInsert(players: Seq[Player]): Future[Seq[Completed]]

  def updateName(stored: Player, given: Player): Future[Seq[Completed]]

  def insert(player: Player): Future[Completed]

  def insertAll(players: Seq[Player]): Future[Completed]

}
