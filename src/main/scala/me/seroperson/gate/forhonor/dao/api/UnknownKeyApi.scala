package me.seroperson.gate.forhonor.dao.api

import me.seroperson.gate.forhonor.model.UnknownKey
import org.mongodb.scala.BulkWriteResult

import scala.concurrent.Future

trait UnknownKeyApi {

  def insertIfIsNotExists(keys: Seq[UnknownKey]): Future[BulkWriteResult]

}
