package me.seroperson.gate.forhonor.di

import akka.actor.{ActorSystem, Scheduler}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import shapeless.tag.@@

import scala.concurrent.ExecutionContext

trait ApplicationComponents {

  val behaviourActorSystem: ActorSystem @@ Tags.Behaviour
  val behaviourMaterializer: ActorMaterializer @@ Tags.Behaviour
  val behaviourExecutionContext: ExecutionContext @@ Tags.Behaviour
  val behaviourScheduler: Scheduler @@ Tags.Behaviour

  val httpClientMaterializer: ActorMaterializer @@ Tags.HttpClient
  val httpClientExecutionContext: ExecutionContext @@ Tags.HttpClient

  val daoExecutionContext: ExecutionContext @@ Tags.DaoExecutionContext

  val httpActorSystem: ActorSystem @@ Tags.HttpServer
  val httpMaterializer: ActorMaterializer @@ Tags.HttpServer
  val httpExecutionContext: ExecutionContext @@ Tags.HttpServer
  val httpScheduler: Scheduler @@ Tags.HttpServer

  val defaultTimeout: Timeout @@ Tags.DefaultTimeout

}
