package me.seroperson.gate.forhonor.di

import akka.actor.{ActorSystem, Scheduler}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import shapeless.tag

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
import scala.concurrent.duration._
import scala.language.postfixOps

class ApplicationComponentsImpl extends ApplicationComponents {

  val behaviourActorSystem = tag[Tags.Behaviour][ActorSystem](ActorSystem("untyped-behaviour-system"))
  val behaviourMaterializer = tag[Tags.Behaviour][ActorMaterializer](ActorMaterializer()(behaviourActorSystem))
  val behaviourExecutionContext = tag[Tags.Behaviour][ExecutionContext](behaviourActorSystem.dispatcher)
  val behaviourScheduler = tag[Tags.Behaviour][Scheduler](behaviourActorSystem.scheduler)

  val httpClientMaterializer = tag[Tags.HttpClient][ActorMaterializer](ActorMaterializer(namePrefix = Some("http-client"))(behaviourActorSystem))
  val httpClientExecutionContext = tag[Tags.HttpClient][ExecutionContext](behaviourActorSystem.dispatchers.lookup("http-client-dispatcher"))

  val daoExecutionContext = tag[Tags.DaoExecutionContext][ExecutionContext](behaviourActorSystem.dispatchers.lookup("dao-dispatcher"))

  val httpActorSystem = tag[Tags.HttpServer][ActorSystem](ActorSystem("http-untyped-system"))
  val httpMaterializer = tag[Tags.HttpServer][ActorMaterializer](ActorMaterializer()(httpActorSystem))
  val httpExecutionContext = tag[Tags.HttpServer][ExecutionContext](httpActorSystem.dispatcher)
  val httpScheduler = tag[Tags.HttpServer][Scheduler](httpActorSystem.scheduler)

  val defaultTimeout = tag[Tags.DefaultTimeout][Timeout](Timeout(30 seconds))

}
