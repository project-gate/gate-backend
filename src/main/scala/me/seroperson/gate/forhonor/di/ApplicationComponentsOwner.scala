package me.seroperson.gate.forhonor.di

trait ApplicationComponentsOwner {

  val applicationComponents: ApplicationComponents

}
