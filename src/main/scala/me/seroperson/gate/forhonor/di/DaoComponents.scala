package me.seroperson.gate.forhonor.di

import com.softwaremill.macwire._
import me.seroperson.gate.forhonor.dao._

trait DaoComponents
  extends ApiComponents
    with DaoMongoComponents {

  lazy val unknownKeyApi = wire[UnknownKeyDao]

}
