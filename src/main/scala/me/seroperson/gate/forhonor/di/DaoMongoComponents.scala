package me.seroperson.gate.forhonor.di

import me.seroperson.gate.forhonor.model._
import org.mongodb.scala.MongoDatabase

import scala.reflect.ClassTag

trait DaoMongoComponents
  extends ApiComponents {

  val db: MongoDatabase

  private def collection[T](name: String)(implicit ct: ClassTag[T]) = db.getCollection[T](name)

  lazy val unknownKeyCollection = collection[UnknownKey]("fh_unknown_key")

}
