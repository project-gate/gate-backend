package me.seroperson.gate.forhonor.di

import com.softwaremill.macwire._
import me.seroperson.gate.forhonor.di.platformed.PlatformedGate
import me.seroperson.gate.forhonor.model.Platform
import me.seroperson.gate.forhonor.service.{MainService, MainServicePowerApiHandler, MainServicePowerApiImpl, OgService, OgServicePowerApi, OgServicePowerApiHandler, OgServicePowerApiImpl}
import org.mongodb.scala.MongoDatabase

class Gate(override val db: MongoDatabase) extends GateComponents {

  override val applicationComponents = new ApplicationComponentsImpl

  import applicationComponents._

  def initServices(platform: Platform) = {
    val platformedGate = new PlatformedGate(
      platform,
      db,
      requestMakerActor,
      unknownKeyService,
      applicationComponents
    )

    val mainImpl = Gate.restrictVisibilityMain(platformedGate)
    val mainHandler = MainServicePowerApiHandler.partial(mainImpl)(
      applicationComponents.httpMaterializer,
      applicationComponents.httpActorSystem
    )

    val ogImpl = Gate.restrictVisibilityOg(platformedGate)
    val ogHandler = OgServicePowerApiHandler.partial(ogImpl)(
      applicationComponents.httpMaterializer,
      applicationComponents.httpActorSystem
    )

    (mainHandler, ogHandler)
  }
}

object Gate {

  private[di] def restrictVisibilityMain(gate: PlatformedGate) = {
    import gate._
    import gate.applicationComponents._
    wire[MainServicePowerApiImpl]
  }

  private[di] def restrictVisibilityOg(gate: PlatformedGate) = {
    import gate._
    import gate.applicationComponents._
    wire[OgServicePowerApiImpl]
  }
}
