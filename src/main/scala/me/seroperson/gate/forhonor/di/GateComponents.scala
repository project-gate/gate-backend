package me.seroperson.gate.forhonor.di

import akka.actor.typed.scaladsl.adapter._
import com.softwaremill.macwire._
import me.seroperson.gate.forhonor.behavior._

trait GateComponents
  extends ApplicationComponentsOwner
    with DaoComponents
    with ServiceComponents {

  import applicationComponents._

  lazy val requestMakerActor =
    behaviourActorSystem.spawn(wire[RequestMaker].behavior, s"request-maker")

}
