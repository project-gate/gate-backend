package me.seroperson.gate.forhonor.di

import com.softwaremill.macwire._
import me.seroperson.gate.forhonor.dao._

trait ServiceComponents
  extends ApplicationComponentsOwner
    with ApiComponents {

  import applicationComponents._

  lazy val unknownKeyService = wire[UnknownKeyDaoService]

}
