package me.seroperson.gate.forhonor.di

object Tags {

  // different akka things
  trait Behaviour
  trait HttpServer
  trait HttpClient

  trait CacheLeaderboardHistory
  trait CacheCurrentLeaderboardInformation
  trait CachePastLeaderboardInformation
  trait CachePastLeaderboardPlayers
  trait CachePlayerInformation
  trait CacheGraph

  // used for different database things
  trait DaoExecutionContext

  // akka.util.Timeout
  trait DefaultTimeout

}
