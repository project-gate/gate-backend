package me.seroperson.gate.forhonor.di.platformed

import me.seroperson.gate.forhonor.dao.api._

trait ApiComponents {

  val historyApi: HistoryApi
  val inconsistencyApi: InconsistencyApi
  val leaderboardApi: LeaderboardApi
  val playerApi: PlayerApi
  val clanApi: ClanApi

}
