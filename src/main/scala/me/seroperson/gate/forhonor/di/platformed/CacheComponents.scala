package me.seroperson.gate.forhonor.di.platformed

import akka.http.caching.LfuCache
import akka.http.caching.scaladsl.{Cache, CachingSettings}
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.di.ApplicationComponentsOwner
import me.seroperson.gate.forhonor.di.Tags._
import me.seroperson.gate.forhonor.model.{InformationChange, Player}
import me.seroperson.gate.forhonor.model.cache.{CurrentLeaderboardInformationCacheKey, GraphCacheKey, PlayerInformationCacheKey}
import me.seroperson.gate.forhonor.service.{PlayerCustomLeaderboard, PlayerRateGraph}
import org.bson.types.ObjectId
import shapeless.tag
import shapeless.tag.@@

import scala.concurrent.duration._
import scala.language.postfixOps

trait CacheComponents
  extends ApplicationComponentsOwner {

  import applicationComponents._

  val defaultCachingSettings = CachingSettings(behaviourActorSystem)

  val leaderboardHistoryCache:
    Cache[Long, List[(Long, List[String])]] @@ CacheLeaderboardHistory = {
    val cachingSettings =
      defaultCachingSettings.withLfuCacheSettings(
        defaultCachingSettings.lfuCacheSettings
          .withInitialCapacity(3)
          .withMaxCapacity(5)
          .withTimeToLive(24 hours)
      )
    tag[CacheLeaderboardHistory](LfuCache(cachingSettings))
  }

  val currentLeaderboardInformationCache:
    Cache[CurrentLeaderboardInformationCacheKey, Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] @@ CacheCurrentLeaderboardInformation = {
    val cachingSettings =
      defaultCachingSettings.withLfuCacheSettings(
        defaultCachingSettings.lfuCacheSettings
          .withInitialCapacity(25)
          .withMaxCapacity(25)
          .withTimeToLive(24 hours)
      )
    tag[CacheCurrentLeaderboardInformation](LfuCache(cachingSettings))
  }

  val pastLeaderboardInformationCache:
    Cache[Int, Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] @@ CachePastLeaderboardInformation = {
    val cachingSettings =
      defaultCachingSettings.withLfuCacheSettings(
        defaultCachingSettings.lfuCacheSettings
          .withInitialCapacity(3)
          .withMaxCapacity(5)
          .withTimeToLive(24 hours)
      )
    tag[CachePastLeaderboardInformation](LfuCache(cachingSettings))
  }

  val pastLeaderboardPlayersCache:
    Cache[Int, Seq[Player]] @@ CachePastLeaderboardPlayers = {
    val cachingSettings =
      defaultCachingSettings.withLfuCacheSettings(
        defaultCachingSettings.lfuCacheSettings
        .withInitialCapacity(5)
        .withMaxCapacity(5)
        .withTimeToLive(24 hours)
      )
    tag[CachePastLeaderboardPlayers](LfuCache(cachingSettings))
  }

  val playerInformationCache:
    Cache[PlayerInformationCacheKey, Seq[InformationChange]] @@ CachePlayerInformation = {
    val cachingSettings =
      defaultCachingSettings.withLfuCacheSettings(
        defaultCachingSettings.lfuCacheSettings
          .withInitialCapacity(128)
          .withMaxCapacity(512)
          .withTimeToLive(24 hours)
      )
    tag[CachePlayerInformation](LfuCache(cachingSettings))
  }

  val perSeasonCustomLeaderboardCache:
    Cache[GraphCacheKey, (PlayerRateGraph, PlayerCustomLeaderboard)] @@ CacheGraph = {
    val cachingSettings =
      defaultCachingSettings.withLfuCacheSettings(
        defaultCachingSettings.lfuCacheSettings
          .withInitialCapacity(128)
          .withMaxCapacity(512)
          .withTimeToLive(24 hours)
      )
    tag[CacheGraph](LfuCache(cachingSettings))
  }

}
