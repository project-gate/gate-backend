package me.seroperson.gate.forhonor.di.platformed

import com.softwaremill.macwire._
import me.seroperson.gate.forhonor.dao._

trait DaoComponents
  extends ApiComponents
    with DaoMongoComponents {

  lazy val historyApi = wire[HistoryDao]
  lazy val inconsistencyApi = wire[InconsistencyDao]
  lazy val leaderboardApi = wire[LeaderboardDao]
  lazy val playerApi = wire[PlayerDao]
  lazy val clanApi = wire[ClanDao]

}
