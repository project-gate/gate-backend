package me.seroperson.gate.forhonor.di.platformed

import me.seroperson.gate.forhonor.model._
import org.mongodb.scala.MongoDatabase

import scala.reflect.ClassTag

trait DaoMongoComponents
  extends ApiComponents
    with PlatformedApplicationComponents {

  val db: MongoDatabase

  private val tablePostfix = "_" + platform.name
  private def collection[T](
    name: String
  )(
    implicit ct: ClassTag[T]
  ) = db.getCollection[T](name + tablePostfix)

  lazy val historyCollection = collection[InformationChange]("fh_player_history")
  lazy val inconsistencyCollection = collection[Inconsistency]("fh_inconsistency")
  lazy val leaderboardCollection = collection[LeaderboardChange]("fh_leaderboard")
  lazy val playerCollection = collection[Player]("fh_player")
  lazy val clanCollection = collection[StoredClan]("fh_clan")

}
