package me.seroperson.gate.forhonor.di.platformed

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.adapter._
import com.softwaremill.macwire._
import me.seroperson.gate.forhonor.behavior._

trait GateComponents
  extends DaoComponents
    with ServiceComponents
    with CacheComponents {

  import applicationComponents._

  lazy val playerStoreActor = behaviourActorSystem.spawn(wire[PlayerStore].behavior, s"player-store-${platform.name}")
  lazy val playerTrackerActor = behaviourActorSystem.spawn(wire[PlayerTracker].behavior, s"player-tracker-${platform.name}")
  lazy val webhookHucksterActor = behaviourActorSystem.spawn(wire[WebhookHuckster].behavior, s"webhook-huckster-${platform.name}")
  lazy val changesReplicatorActor = behaviourActorSystem.spawn(wire[ChangesReplicator].behavior, s"changes-replicator-${platform.name}")

  val requestMakerActor: ActorRef[RequestMaker.Command]

}
