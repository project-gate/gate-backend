package me.seroperson.gate.forhonor.di.platformed

import me.seroperson.gate.forhonor.model.Platform

trait PlatformedApplicationComponents {

  val platform: Platform

}
