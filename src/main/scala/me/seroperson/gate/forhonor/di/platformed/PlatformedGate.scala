package me.seroperson.gate.forhonor.di.platformed
import akka.actor.typed.ActorRef
import me.seroperson.gate.forhonor.behavior.RequestMaker
import me.seroperson.gate.forhonor.dao.UnknownKeyDaoService
import me.seroperson.gate.forhonor.di.ApplicationComponents
import me.seroperson.gate.forhonor.model.Platform
import org.mongodb.scala.MongoDatabase

class PlatformedGate(
  override val platform: Platform,
  override val db: MongoDatabase,
  override val requestMakerActor: ActorRef[RequestMaker.Command],
  override val unknownKeyService: UnknownKeyDaoService,
  override val applicationComponents: ApplicationComponents
) extends GateComponents
