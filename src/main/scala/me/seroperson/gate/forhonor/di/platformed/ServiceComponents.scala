package me.seroperson.gate.forhonor.di.platformed

import com.softwaremill.macwire._
import me.seroperson.gate.forhonor.dao._
import me.seroperson.gate.forhonor.di.ApplicationComponentsOwner

trait ServiceComponents
  extends ApplicationComponentsOwner
    with PlatformedApplicationComponents
    with ApiComponents {

  import applicationComponents._

  lazy val historyService = wire[HistoryDaoService]
  lazy val inconsistencyService = wire[InconsistencyDaoService]
  lazy val leaderboardService = wire[LeaderboardDaoService]
  lazy val playerService = wire[PlayerDaoService]
  lazy val clanService = wire[ClanDaoService]

  val unknownKeyService: UnknownKeyDaoService

}
