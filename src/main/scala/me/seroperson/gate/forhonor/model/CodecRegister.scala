package me.seroperson.gate.forhonor.model

import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.bson.codecs.configuration.CodecRegistries.{fromRegistries, fromProviders}

object CodecRegister {

  val allRegistries = fromRegistries(
    fromProviders(
      classOf[Player],
      classOf[Profile],
      classOf[Activity],
      classOf[InformationChange],
      classOf[TotalInformationResponse],
      classOf[TotalInformationResponseStat],
      classOf[LiveCapturedResponse],
      classOf[LiveCapturedResponseIteration],
      classOf[LiveCapturedResponseStat],
      classOf[LeaderboardChange],
      classOf[Change],
      classOf[StoredClan]
    ),
    DEFAULT_CODEC_REGISTRY
  )

}
