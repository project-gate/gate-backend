package me.seroperson.gate.forhonor.model

import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.annotations.BsonProperty

case class Inconsistency(
  @BsonProperty("_id") id: ObjectId
)
