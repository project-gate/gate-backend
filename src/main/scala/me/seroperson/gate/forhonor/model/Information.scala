package me.seroperson.gate.forhonor.model

import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.annotations.BsonProperty

case class InformationChange(
  @BsonProperty("_id") id: ObjectId,
  @BsonProperty("player_id") playerId: ObjectId,
  timestamp: Long,
  key: Int,
  value: Double,
  @BsonProperty("is_first_fetch") isFirstFetch: Boolean
)

// Aggregation models

// 'total information' request
case class TotalInformationResponse(
  @BsonProperty("_id") playerId: ObjectId,
  keys: Seq[TotalInformationResponseStat]
)

case class TotalInformationResponseStat(
  key: Int,
  value: Double
)

// 'live captured' request
case class LiveCapturedResponse(
  @BsonProperty("_id") playerId: ObjectId,
  iterations: Seq[LiveCapturedResponseIteration]
)

case class LiveCapturedResponseIteration(
  timestamp: Long,
  changes: Seq[LiveCapturedResponseStat]
)

case class LiveCapturedResponseStat(
  key: Int,
  value: Double
)
