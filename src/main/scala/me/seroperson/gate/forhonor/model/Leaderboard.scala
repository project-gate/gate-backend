package me.seroperson.gate.forhonor.model

import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.annotations.BsonProperty

case class LeaderboardChange(
  @BsonProperty("_id") id: ObjectId,
  timestamp: Long,
  @BsonProperty("change_type") changeType: Change
)

sealed class Change

case class Inserted(
  /*@BsonProperty("profile_id")*/ profileId: String,
  to: Int
) extends Change

case class Removed(
  from: Int
) extends Change

case class Moved(
  from: Int,
  to: Int
) extends Change
