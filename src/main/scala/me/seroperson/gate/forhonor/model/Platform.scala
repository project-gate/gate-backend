package me.seroperson.gate.forhonor.model

/* just logical model. not for database */

case class Platform(
  name: String,
  id: String,
  spaceId: String,
  sandboxId: String
) {
  override def toString = name
}
