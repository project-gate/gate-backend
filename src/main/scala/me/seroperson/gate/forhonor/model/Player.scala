package me.seroperson.gate.forhonor.model

import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.annotations.BsonProperty

case class Player(
  @BsonProperty("_id") id: ObjectId,
  name: String,
  profile: Profile,
  activity: Activity,
  @BsonProperty("tracking_source") trackingSource: Int,
  @BsonProperty("name_history") nameHistory: List[String]
)

case class Profile(
  id: String
)

case class Activity(
  @BsonProperty("last_activity") lastActivity: Option[Long],
  @BsonProperty("last_activity_check") lastActivityCheck: Option[Long],
  @BsonProperty("first_activity") firstActivity: Option[Long],
  @BsonProperty("inactivity_combo") inactivityCombo: Int
)

object Player {
  // 0 - tracked by project admin
  // 1 - leaderboard-autopopulation
  // 2 - tracked by MainService/trackUser
  final val SourceProjectAdmin = 0
  final val SourceLeaderboardAutopopulation = 1
  final val SourceTrackUser = 2
}
