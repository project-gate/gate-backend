package me.seroperson.gate.forhonor.model

import me.seroperson.gate.forhonor.util.ConfigInstance

import scala.jdk.CollectionConverters._

/* just logical model. not for database */

case class Season(
  number: Int,
  title: String,
  startTimestamp: Long,
  startLeaderboardTimestamp: Long,
  ignoreLeaderboard: Boolean
)

trait SeasonSyntax {

  implicit class SeasonExtended(season: Season) {

    def nextSeason(allSeasons: List[Season]) = {
      allSeasons.find(_.number > season.number)
    }

    def previousSeason(allSeasons: List[Season]) = {
      allSeasons.findLast(_.number < season.number)
    }

    def endTimestamp(allSeasons: List[Season]) = {
      nextSeason(allSeasons).map(_.startTimestamp).getOrElse(Long.MaxValue)
    }

    def endLeaderboardTimestamp(allSeasons: List[Season]) = {
      nextSeason(allSeasons).map(_.startLeaderboardTimestamp).getOrElse(Long.MaxValue)
    }
  }

}

object Season extends SeasonSyntax {
  val AllSeasons = ConfigInstance
    .Gate
    .Root
    .getConfigList("season-bounds")
    .asScala
    .map { platformSeasonsConfig =>
      val platformName = platformSeasonsConfig.getString("platform-name")
      platformName -> {
        platformSeasonsConfig
          .getConfigList("seasons")
          .asScala
          .map { seasonConfig =>
            val number = seasonConfig.getInt("number")
            val title = seasonConfig.getString("title")
            val startTimestamp = seasonConfig.getLong("start-timestamp")
            val startLeaderboardTimestamp = {
              if(seasonConfig.hasPath("start-leaderboard-timestamp")) {
                seasonConfig.getLong("start-leaderboard-timestamp")
              } else {
                startTimestamp
              }
            }
            val ignoreLeaderboard = {
              if(seasonConfig.hasPath("ignore-leaderboard")) {
                seasonConfig.getBoolean("ignore-leaderboard")
              } else {
                false
              }
            }
            Season(
              number,
              title,
              startTimestamp,
              startLeaderboardTimestamp,
              ignoreLeaderboard
            )
          }
          .toList
      }
    }
    .toMap
}
