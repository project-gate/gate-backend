package me.seroperson.gate.forhonor.model

import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.annotations.BsonProperty

case class StoredClan(
  @BsonProperty("_id") id: ObjectId,

  // metadata
  @BsonProperty("name_id") nameId: String,
  @BsonProperty("name_view") nameView: String,
  @BsonProperty("short_description") shortDescription: String,
  @BsonProperty("long_description") longDescription: Option[String],
  image: Option[String],
  country: String,
  @BsonProperty("created_timestamp") createdTimestamp: Long,

  // members
  @BsonProperty("leader_player_id") leaderPlayerId: ObjectId,
  @BsonProperty("by_regex") byRegex: List[String],
  @BsonProperty("by_player_id") byPlayerId: List[ObjectId],
  @BsonProperty("exclude_by_player_id") excludedByPlayerId: List[ObjectId]
)
