package me.seroperson.gate.forhonor.model

import org.mongodb.scala.bson.annotations.BsonProperty

case class UnknownKey(
  @BsonProperty("_id") id: String
)
