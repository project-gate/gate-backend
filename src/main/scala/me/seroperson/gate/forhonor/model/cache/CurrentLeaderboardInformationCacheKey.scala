package me.seroperson.gate.forhonor.model.cache

case class CurrentLeaderboardInformationCacheKey(
  profileIds: Iterable[String],
  toTimestamp: Long
)
