package me.seroperson.gate.forhonor.model.cache

import me.seroperson.gate.forhonor.model.Season

case class GraphCacheKey(
  season: Season
)
