package me.seroperson.gate.forhonor.model.cache

import org.bson.types.ObjectId

case class PlayerInformationCacheKey(
  playerId: ObjectId,
  toTimestamp: Long
)
