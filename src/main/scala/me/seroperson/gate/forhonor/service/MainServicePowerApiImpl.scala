package me.seroperson.gate.forhonor.service

import akka.actor.typed.ActorRef
import akka.actor.ActorSystem
import akka.http.caching.scaladsl.Cache
import akka.stream.Materializer
import akka.util.Timeout
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.behavior.{PlayerStore, PlayerTracker, RequestMaker}
import me.seroperson.gate.forhonor.dao.{ClanDaoService, HistoryDaoService, LeaderboardDaoService, PlayerDaoService}
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.di.Tags.CacheGraph
import me.seroperson.gate.forhonor.model.{InformationChange, Platform, Player}
import me.seroperson.gate.forhonor.model.cache.{CurrentLeaderboardInformationCacheKey, GraphCacheKey, PlayerInformationCacheKey}
import me.seroperson.gate.forhonor.service.method._
import org.bson.types.ObjectId
import shapeless.tag.@@

import scala.concurrent.ExecutionContext
import scala.language.postfixOps

class MainServicePowerApiImpl(
  val platform: Platform,
  val requestMaker: ActorRef[RequestMaker.Command],
  val playerStore: ActorRef[PlayerStore.Command],
  val playerTracker: ActorRef[PlayerTracker.Command],
  val playerDaoService: PlayerDaoService,
  val leaderboardDaoService: LeaderboardDaoService,
  val historyDaoService: HistoryDaoService,
  val clanDaoService: ClanDaoService,
  val leaderboardHistoryCache:
    Cache[Long, List[(Long, List[String])]] @@ Tags.CacheLeaderboardHistory,
  val currentLeaderboardInformationCache:
    Cache[CurrentLeaderboardInformationCacheKey, Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] @@ Tags.CacheCurrentLeaderboardInformation,
  val pastLeaderboardInformationCache:
    Cache[Int, Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] @@ Tags.CachePastLeaderboardInformation,
  val pastLeaderboardPlayersCache:
    Cache[Int, Seq[Player]] @@ Tags.CachePastLeaderboardPlayers,
  val playerInformationCache:
    Cache[PlayerInformationCacheKey, Seq[InformationChange]] @@ Tags.CachePlayerInformation,
  val perSeasonCustomLeaderboardCache:
    Cache[GraphCacheKey, (PlayerRateGraph, PlayerCustomLeaderboard)] @@ CacheGraph,
  val actorSystem: ActorSystem @@ Tags.HttpServer,
  val materializer: Materializer @@ Tags.HttpServer,
  val executionContext: ExecutionContext @@ Tags.HttpServer,
  val schedulerClassic: akka.actor.Scheduler @@ Tags.HttpServer,
  val timeout: Timeout @@ Tags.DefaultTimeout
) extends MainServicePowerApi
  with TrackUser
  with GetPlayers
  with GetGraphs
  with GetHistory
  with GetGlobalStatistics
  with GetLeaderboardActivity
  with GetLeaderboardHistory
  with GetClans
  with GetClanInfo
