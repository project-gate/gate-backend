package me.seroperson.gate.forhonor.service

import akka.actor.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.adapter._
import akka.http.caching.scaladsl.Cache
import akka.stream.Materializer
import akka.util.Timeout
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.behavior.{PlayerStore, PlayerTracker, RequestMaker}
import me.seroperson.gate.forhonor.dao.{ClanDaoService, HistoryDaoService, LeaderboardDaoService, PlayerDaoService}
import me.seroperson.gate.forhonor.di.Tags
import me.seroperson.gate.forhonor.di.Tags.{CacheCurrentLeaderboardInformation, CacheGraph, CacheLeaderboardHistory, CachePastLeaderboardInformation, CachePastLeaderboardPlayers, CachePlayerInformation}
import me.seroperson.gate.forhonor.model.{InformationChange, Platform, Player}
import me.seroperson.gate.forhonor.model.cache.{CurrentLeaderboardInformationCacheKey, GraphCacheKey, PlayerInformationCacheKey}
import org.bson.types.ObjectId
import shapeless.tag.@@

import scala.concurrent.ExecutionContext

trait ServicePowerApiArgs {

  val platform: Platform
  val requestMaker: ActorRef[RequestMaker.Command]
  val playerStore: ActorRef[PlayerStore.Command]
  val playerTracker: ActorRef[PlayerTracker.Command]
  val playerDaoService: PlayerDaoService
  val leaderboardDaoService: LeaderboardDaoService
  val historyDaoService: HistoryDaoService
  val clanDaoService: ClanDaoService

  val leaderboardHistoryCache:
    Cache[Long, List[(Long, List[String])]] @@ CacheLeaderboardHistory
  val currentLeaderboardInformationCache:
    Cache[CurrentLeaderboardInformationCacheKey, Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] @@ CacheCurrentLeaderboardInformation
  val pastLeaderboardInformationCache:
    Cache[Int, Map[ObjectId, Map[Long, Seq[InformationChangeParsed]]]] @@ CachePastLeaderboardInformation
  val pastLeaderboardPlayersCache:
    Cache[Int, Seq[Player]] @@ CachePastLeaderboardPlayers
  val playerInformationCache:
    Cache[PlayerInformationCacheKey, Seq[InformationChange]] @@ CachePlayerInformation
  val perSeasonCustomLeaderboardCache:
    Cache[GraphCacheKey, (PlayerRateGraph, PlayerCustomLeaderboard)] @@ CacheGraph

  val actorSystem: ActorSystem @@ Tags.HttpServer
  val materializer: Materializer @@ Tags.HttpServer
  val executionContext: ExecutionContext @@ Tags.HttpServer
  val schedulerClassic: akka.actor.Scheduler @@ Tags.HttpServer
  val timeout: Timeout @@ Tags.DefaultTimeout

  implicit val ac = actorSystem
  implicit val mat = materializer
  implicit val ec = executionContext
  implicit val sc = schedulerClassic
  implicit val t = timeout
  implicit val scTyped = actorSystem.toTyped.scheduler

}
