package me.seroperson.gate.forhonor.service

import akka.grpc.GrpcServiceException
import io.grpc.Status
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed._
import me.seroperson.gate.forhonor.model.Game._
import me.seroperson.gate.forhonor.model._
import me.seroperson.gate.forhonor.model.cache.{CurrentLeaderboardInformationCacheKey, PlayerInformationCacheKey}

import scala.annotation.tailrec
import scala.concurrent.Future
import scala.language.postfixOps

trait Shared extends ServicePowerApiArgs with SharedHelper {

  val allSeasons = Season.AllSeasons.getOrElse(platform.name, List.empty)
  val lastSeason = allSeasons.lastOption
  val allLeaderboardSeasons = allSeasons.filterNot(_.ignoreLeaderboard)

  private val profileIdRegex = """\w{8}-\w{4}-\w{4}-\w{4}-\w{12}""".r

  def getPlayersByRequest(request: Option[HistoryRequestType]): Future[Seq[Player]] = {
    request
      .map(_.requestFrom)
      .getOrElse(HistoryRequestType.RequestFrom.Empty) match {
        case HistoryRequestType.RequestFrom.Empty =>
          Future.failed(
            new GrpcServiceException(
              Status.INVALID_ARGUMENT
                .withDescription("Unable to parse request.")
            )
          )
        case HistoryRequestType.RequestFrom.PlayerId(id) =>
          getPlayerByIdentity(Some(id)).map(v => Seq(v))
        case HistoryRequestType.RequestFrom.ClanId(clanId) =>
          getClanById(clanId)
            .flatMap { clan =>
              clanDaoService
                .findPlayers(clan)
                .map { case (leader, players) =>
                  leader.toSeq ++ players
                }
            }
      }
  }

  def getClanById(id: String) = {
    clanDaoService
      .findByNameId(id)
      .flatMap { clanO =>
        clanO match {
          case Some(clan) =>
            Future.successful(clan)
          case None =>
            Future.failed(
              new GrpcServiceException(
                Status.INVALID_ARGUMENT
                  .withDescription("Unknown clan identifier.")
              )
            )
        }
      }
  }

  def getPlayerByIdentity(idO: Option[PlayerIdentity]) =
    (idO
      .map(_.id)
      .getOrElse(PlayerIdentity.Id.Empty) match {
        case PlayerIdentity.Id.ProfileId(id) if profileIdRegex.findFirstMatchIn(id).isDefined =>
          playerDaoService.findByProfileId(id)
        case PlayerIdentity.Id.Name(name) =>
          playerDaoService.findByName(name)
        case PlayerIdentity.Id.Empty =>
          Future.failed(
            new GrpcServiceException(
              Status.INVALID_ARGUMENT
                .withDescription("Unable to parse request.")
            )
          )
      })
    .flatMap { playerO =>
      playerO match {
        case Some(player) =>
          Future.successful(player)
        case None =>
          Future.failed(
            new GrpcServiceException(
              Status.INVALID_ARGUMENT
                .withDescription("This player is not tracked yet.")
            )
          )
      }
    }

  // todo init dynamically
  val rankedKeys = Seq(
    301134897,1072094004,-2058172112,-1634525390,-1287213005,648447282,292946950,-629736633,-372717369,1215630533,2076882761,-1295400952,
    -522990455,1177750024,-1416476793,1451817896,-1818455479,-793761558,-1254801498,794586344,-230107577,-2033703248,2124131053,1280125551,
    -445355346,-308222351,535783151,-203577829,453653723,1478347644,-1228271750,2042001625,-1791925731,-1257674732,2031413649,-1370067309,
    443065747,1336552085,330673170,2025094329,-569132488,324353850,-272138072,1663522215,-695784794,1724442996,262862997,136095094,1597675093,
    -1325484905,-1108944301,1270843906,-1106009727,941078145,1600609667,-1765541249,-1435775488,-1195993430,392354472,2069614568,436810569,
    2025158471,-1450719598,1143507219,-557233260,1238376559,-349971343,-223203440,-1684783439,-1811551342,1021835955,-92115124,1496232778,
    830568459,-953367352,1753252042,-1876050935,1317909803,-270438099,652245484,1574929067,-1131690327,-2054373910,54625664,-1646114815,
    948112002,1362435904,-1792880129,2053390528,-1344183490,465042626,913739265,1133368030,684671391,235974752,-514478775,1421181512,
    -861791160,1844828234,-1285437882,1073869127,-1276494504,311853398,673805751,-82513388,-1670861290,-2032813643,-117128807,331567832,
    -565825446,2022186428,-1613249346,-1943015107,433838526,763604287,-1516752401,-594068818,-851088082,71595501,1855531312,994279084,
    -255462099,-1980942996,-392595094,1332885803,-1236600596,1470018798,1759837767,-289550075,735143846
  )

  def findMostPlayedHero(changes: Map[Long, Seq[InformationChangeParsed]]): Hero = {
    findMostPlayedHero(
      changes
        .values
        .flatten
    )
  }

  def findMostPlayedHero(changes: Traversable[InformationChangeParsed]): Hero = {
    val changesByHero = changes.groupBy(_.hero)
    if (changesByHero.isEmpty) {
      Hero.HeroUnspecified
    } else {
      changesByHero
        .maxBy { case (hero, changes) =>
          changes.map(_.value).sum
        }
        ._1
    }
  }

  implicit class ExtendedList[A](seq: List[A]) {
    def dropWhileWithAcc[B](z: B, `while`: B => Boolean)(op: (A, B) => B): List[A] = {
      @tailrec
      def rec(trav: List[A], z: B): List[A] = trav match {
        case head :: tail if `while`(z) => rec(tail, op(head, z))
        case _ => trav
      }
      rec(seq, z)
    }
    def takeWhileWithAcc[B](z: B, `while`: B => Boolean)(op: (A, B) => B): List[A] = {
      def rec(trav: List[A], z: B): List[A] = trav match {
        case head :: tail if `while`(z) => head +: rec(tail, op(head, z))
        case _ => List.empty
      }
      rec(seq, z)
    }
  }

  def findCurrentRankedMatchHistoryByPlayer(
    players: Seq[Player],
    fromTimestamp: Long,
    toTimestamp: Long
  ) = {
    val newCacheKey = CurrentLeaderboardInformationCacheKey(
      players.map(_.profile.id).toSet,
      toTimestamp
    )
    currentLeaderboardInformationCache
      .put(
        newCacheKey,
        currentLeaderboardInformationCache
          .keys
          .toList
          .sortBy(_.toTimestamp * -1)
          .headOption
          .flatMap { oldCacheKey =>
            if (oldCacheKey.profileIds == newCacheKey.profileIds) {
              currentLeaderboardInformationCache
                .get(oldCacheKey)
                .map {
                  oldCacheKey -> _
                }
            } else {
              currentLeaderboardInformationCache.clear()
              None
            }
          }
          .map { case ((oldCacheKey, cachedInformationF)) =>
            (for {
              cachedInformation <- cachedInformationF
              newInformationF = historyDaoService.findLiveCaptured(
                rankedKeys,
                players,
                oldCacheKey.toTimestamp,
                newCacheKey.toTimestamp
              )
              newInformation <- newInformationF
            } yield {
              newInformation.foldLeft(cachedInformation) { case (updated, (playerId, information)) =>
                updated.updated(playerId, information ++ updated.getOrElse(playerId, Map.empty))
              }
            })
              .map { result =>
                currentLeaderboardInformationCache.remove(oldCacheKey)
                result
              }
          }
          .getOrElse {
            historyDaoService.findLiveCaptured(rankedKeys, players, fromTimestamp, toTimestamp)
          }
      )
  }

  def findSeasonFirstIteration(history: List[(Long, List[String])], season: Season) = {
    history
      .dropWhile { case (elementTimestamp, _) =>
        elementTimestamp < season.startLeaderboardTimestamp
      }
      .headOption
      .map { case (elementTimestamp, _) =>
        elementTimestamp
      }
  }

  def getOrUpdateParsedHistory(
    players: Seq[Player],
    firstTimestamp: Long,
    lastTimestamp: Long,
    skip: Int,
    limit: Int
  ) = {
    val allCapturedKeys = playerInformationCache.keys
    val previouslyCapturedPlayers = allCapturedKeys
      .flatMap { key =>
        players
          .find(_.id == key.playerId)
          .map(_ -> key)
      }
    val partiallyCaptured = previouslyCapturedPlayers
      .filter { case (player, key) =>
        key.toTimestamp < lastTimestamp
      }
    val fullyCaptured = previouslyCapturedPlayers.diff(partiallyCaptured)
    val uncaptured = players/*players
      .filterNot(player => allCapturedKeys.exists(_.playerId == player.id))*/

    // firstly we need to fetch partially captured
    val dataFromPartiallyCapturedF = (if(partiallyCaptured.nonEmpty) {
      historyDaoService
        // requesting updates for each partially captured player
        .findByPlayerIntervalIds(
          partiallyCaptured
            .map { case (_, key) =>
              key.playerId -> (key.toTimestamp, lastTimestamp)
            }
            .toSeq
        )
        .flatMap { fetchedChanges =>
          // now updating each cache
          Future
            .traverse(fetchedChanges.groupBy(_.playerId).toList) { case (playerId, changes) =>
              val (player, oldCacheKey) = partiallyCaptured
                .find { case (_, key) =>
                  key.playerId == playerId
                }
                // actually it should be impossible
                .getOrElse(throw new IllegalStateException("oldCacheKey is missing during updating caches"))
              playerInformationCache
                .get(oldCacheKey)
                .map { oldChangesF =>
                  playerInformationCache
                    .put(
                      PlayerInformationCacheKey(playerId, lastTimestamp),
                      oldChangesF.map { oldChanges =>
                        playerInformationCache.remove(oldCacheKey)
                        oldChanges ++ changes
                      }
                    )
                }
                // it can be possible if cache is expired during the operations above
                .getOrElse(Future.successful(Seq.empty))
                .map(player -> _)
            }
            .map(_.toSeq)
        }
    } else {
      Future.successful(Seq.empty)
    })

    // now fetching the same for fully captured players
    val dataFromFullyCapturedF = Future
      .traverse(fullyCaptured) { case (player, key) =>
        playerInformationCache
          .get(key)
          // the same, it can be possible if cache is expired
          .getOrElse(Future.successful(Seq.empty))
          .map(player -> _)
      }
      .map(_.toSeq)

    // now we need to fetch partial data for players which are not captured at all
    val partialDataF = historyDaoService
      .findPaginatedLiveCaptured(uncaptured, firstTimestamp, skip, limit)
      .map { result =>
        result.flatMap { case (playerId, changes) =>
          players
            .find(_.id == playerId)
            .map { player =>
              player -> changes.filter { case (_, timestampChanges) =>
                timestampChanges.nonEmpty
              }
            }
        }
      }

    for {
      // dataFromPartiallyCaptured <- dataFromPartiallyCapturedF
      // dataFromFullyCaptured <- dataFromFullyCapturedF
      partialData <- partialDataF
      /*partiallyDataFromCache <- Future
        .traverse(dataFromPartiallyCaptured ++ dataFromFullyCaptured) { case (player, changes) =>
          Future
            .sequence(
              changes
                .map(
                  _
                    .toParsed()
                    .map { changes =>
                      player -> changes
                        .toSeq
                        .filter { change =>
                          val notFirstFetch = !change.isFirstFetch
                          val beforeFirstTimestamp = change.timestamp < firstTimestamp
                          notFirstFetch && beforeFirstTimestamp
                        }
                        .toList
                        .sortBy(_.timestamp * -1)
                        .dropWhileWithAcc[Set[Long]](
                          Set.empty,
                          _.size < skip
                        ) { case (change, acc) =>
                          acc + change.timestamp
                        }
                        .takeWhileWithAcc[Set[Long]](
                          Set.empty,
                          _.size < limit
                        ) { case (change, acc) =>
                          acc + change.timestamp
                        }
                        .groupBy(_.timestamp)
                    }
                )
            )
        }
        .map(
          _
            .flatten
            .toMap
        )*/
    } yield {
      partialData // ++ partiallyDataFromCache
    }
  }

  def getOrUpdateParsedHistory(player: Player) = {
    val newCacheKey = PlayerInformationCacheKey(player.id, System.currentTimeMillis())
    playerInformationCache
      .put(
        newCacheKey,
        playerInformationCache
          .keys
          .find(_.playerId == player.id)
          .flatMap { oldCacheKey =>
            playerInformationCache
              .get(oldCacheKey)
              .map {
                _
                  .flatMap { cachedInformation =>
                    historyDaoService
                      .findByPlayerId(player.id, oldCacheKey.toTimestamp, newCacheKey.toTimestamp)
                      .map { newInformation =>
                        playerInformationCache.remove(oldCacheKey)
                        cachedInformation ++ newInformation
                      }
                  }
              }
          }
          .getOrElse {
            historyDaoService.findByPlayerId(player.id, newCacheKey.toTimestamp)
          }
      )
      .flatMap { changes =>
        Future
          .sequence(changes.map(_.toParsed()))
          .map(
            _
              .flatten
              .toSeq
          )
      }
  }

  def toGlobalStatisticsActivityElements(player: Player, historyParsed: Map[Long, Seq[InformationChangeParsed]]) = {
    historyParsed
      .map { case (timestamp, timestampChanges) =>
        GlobalStatisticsActivity(
          player.name,
          player.profile.id,
          timestamp,
          timestampChanges
            .groupBy(_.hero)
            .flatMap { case (hero, heroChanges) =>
              heroChanges
                .groupBy(_.game)
                .flatMap { case (game, gameChanges) =>
                  gameChanges
                    .groupBy(_.qualifier)
                    .map { case (qualifier, qualifierChanges) =>
                      GlobalStatisticsActivityElement(
                        qualifier,
                        hero,
                        game,
                        fetchStatValue(qualifierChanges)(humanKillAssistPredicate),
                        fetchStatValue(qualifierChanges)(botKillAssistPredicate),
                        fetchStatValue(qualifierChanges)(minionKillPredicate),
                        fetchStatValue(qualifierChanges)(deathPredicate),
                        fetchStatValue(qualifierChanges)(winCountPredicate),
                        fetchStatValue(qualifierChanges)(looseCountPredicate)
                      )
                    }
                }
            }
            .toSeq
        )
      }
      .toSeq
      .sortBy(-_.timestamp)
  }

  def gameToDuelsEq(game: Game) =
    game match {
      case Duel => 1.0
      case RankedDuel => 1.0
      case Dominion => 4.0
      case Breach => 6.0
      case Brawl => 1.5
      case TeamDeathmatch => 2.5
      case TeamLastManStanding => 2.5
      case Tribute => 2.5
      case Kampaign => 4.0
      case Arcade => 3.0
      case Training => 2.0
      case _ => 1.0
    }
}
