package me.seroperson.gate.forhonor.service

import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.model._

import scala.language.postfixOps

trait SharedHelper {

  def playerToPlayerElement(player: Player) = {
    val lastActivity = player.activity.lastActivity.getOrElse(-1L)
    PlayerElement(
      player.name,
      player.profile.id,
      lastActivity
    )
  }

  def playersToPlayerElements(players: Seq[Player]) = {
    players
      .filter(_.activity.lastActivity.isDefined)
      .sortBy(_.activity.lastActivity.map(_ * -1))
      .map(playerToPlayerElement)
  }

  def fetchStatValue(changes: IterableOnce[InformationChangeParsed])(selector: InformationChangeParsed => Boolean) =
    changes.filter(selector).map(_.value).fold(0.0)(_ + _).toLong

}
