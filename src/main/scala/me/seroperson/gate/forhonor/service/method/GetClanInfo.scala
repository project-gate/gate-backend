package me.seroperson.gate.forhonor.service.method

import akka.grpc.GrpcServiceException
import akka.grpc.scaladsl.Metadata
import io.grpc.Status
import me.seroperson.gate.forhonor.model._
import me.seroperson.gate.forhonor.service._

import scala.concurrent.Future

trait GetClanInfo extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getClanInfo(in: ClanInfoRequest, metadata: Metadata): Future[ClanInfoReply] = {
    for {
      clan <- clanDaoService
        .findByNameId(in.id)
        .flatMap {
          _
            .fold[Future[StoredClan]](
              Future.failed(
                new GrpcServiceException(
                  Status.INVALID_ARGUMENT
                    .withDescription("Unknown clan identifier.")
                )
              )
            )(Future.successful)
        }
      (leader, players) <- clanDaoService.findPlayers(clan)
      changes <- getOrUpdateParsedHistory(leader.toSeq ++ players, Long.MaxValue, Long.MinValue, 0, 20)
    } yield {
      ClanInfoReply(
        clan.nameId,
        clan.nameView,
        clan.image.getOrElse(""),
        clan.longDescription.getOrElse(clan.shortDescription),
        clan.country,
        clan.createdTimestamp,
        leader.map(playerToPlayerElement),
        playersToPlayerElements(players)
          .sortBy(_.name),
        changes
          .map { case(player, changes) =>
            toGlobalStatisticsActivityElements(player, changes)
          }
          .flatten
          .toList
          .sortBy(-_.timestamp)
      )
    }
  }

}
