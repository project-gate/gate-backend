package me.seroperson.gate.forhonor.service.method

import akka.grpc.scaladsl.Metadata
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed._
import me.seroperson.gate.forhonor.model.Season._
import me.seroperson.gate.forhonor.model.cache.PlayerInformationCacheKey
import me.seroperson.gate.forhonor.model.{Game, Hero, Season}
import me.seroperson.gate.forhonor.service._

import scala.collection.MapView
import scala.concurrent.Future

trait GetClans extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getClans(in: ClansRequest, metadata: Metadata): Future[ClansReply] = {
    clanDaoService
      .findAll()
      .flatMap { clans =>
        Future
          .traverse(clans) { clan =>
            clanDaoService
              .findPlayers(clan)
              .map { players =>
                clan -> players
              }
          }
      }
      .map { result =>
        ClansReply(
          result
            .map { case (clan, (leader, players)) =>
              Clan(
                clan.nameId,
                clan.nameView,
                clan.shortDescription,
                clan.country,
                playersToPlayerElements(leader.toSeq ++ players)
                  .sortBy(_.name)
              )
            }
        )
      }
  }

}
