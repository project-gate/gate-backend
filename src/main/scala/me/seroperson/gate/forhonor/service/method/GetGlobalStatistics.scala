package me.seroperson.gate.forhonor.service.method

import akka.grpc.scaladsl.Metadata
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed._
import me.seroperson.gate.forhonor.model.Season._
import me.seroperson.gate.forhonor.model.cache.PlayerInformationCacheKey
import me.seroperson.gate.forhonor.model.{Game, Hero, Season}
import me.seroperson.gate.forhonor.service._

import scala.collection.MapView
import scala.concurrent.Future

trait GetGlobalStatistics extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getGlobalStatistics(in: GlobalStatisticsRequest, metadata: Metadata): Future[GlobalStatisticsReply] = {
    for {
      player <- getPlayerByIdentity(in.id)
      historyParsed <- getOrUpdateParsedHistory(player)
      leaderboardHistory <- leaderboardDaoService.count().flatMap { key =>
        leaderboardHistoryCache.getOrLoad(
          key,
          _ => leaderboardDaoService.composeLeaderboardHistory()
        )
      }
    } yield {
      def groupByTimestamp(history: Iterable[InformationChangeParsed]) = {
        val initialState = history.groupBy { element =>
          def checkForBounds(season: Season) = {
            season.startTimestamp < element.timestamp && element.timestamp < season.endTimestamp(allSeasons) && !element.isFirstFetch
          }
          allSeasons
            .find(checkForBounds)
            .map { season =>
              Some(season)
            }
            .flatten
        }
        val withoutOverall = initialState.view.filterKeys(_ != None).toMap
        withoutOverall.concat(
          Set(
            None -> (initialState.getOrElse(None, Seq.empty) ++ withoutOverall.values.flatten)
          )
        )
      }
      def calculateDoubles(element: GlobalStatisticsElement) =
        element
          .copy(
            // https://stackoverflow.com/questions/11701399/round-up-to-2-decimal-places-in-java
            kd = if(element.deaths > 0) {
              Math.round(element.kills.toDouble / element.deaths.toDouble * 100.0) / 100.0
            } else {
              -1.0
            },
            wl = if(element.losses > 0) {
              Math.round(element.wins.toDouble / element.losses.toDouble * 100.0) / 100.0
            } else {
              -1.0
            }
          )
      def createCategory(
        gameO: Option[Game],
        countInDuels: Double,
        totalDuelsCount: Double, // per season
        boundedHistory: Iterable[InformationChangeParsed]
      ) = {
        val singleGames = boundedHistory.filterNot(_.isFirstFetch)
          // matches per game, timestamp
          .groupBy(_.timestamp)
          .view
          .mapValues { gamesByTimestamp =>
            gamesByTimestamp
              // matches per game, timestamp, hero
              .groupBy(_.hero)
              .view
              .mapValues { gamesByHero =>
                // matches per game, timestamp, hero
                gamesByHero
                  // matches per game, timestamp, hero, qualifier
                  .groupBy(_.qualifier)
                  .filter { case (qualifier, gamesByQualifier) =>
                    gamesByQualifier.isSingleGame()
                  }
                  .values
                  .flatten
              }
              .values
              .flatten
          }
        def findWithinSingleGamesMaxMinBy(
          singleGames: MapView[Long, Iterable[InformationChangeParsed]],
          maxPredicate: InformationChangeParsed => Boolean,
          minPredicate: InformationChangeParsed => Boolean
        ) = singleGames
          .flatMap { case (timestamp, games) =>
            games
              .find(maxPredicate)
              .map { topPerformanceStat =>
                Tuple2(
                  timestamp,
                  topPerformanceStat -> games
                )
              }
          }
          .toMap
          .values
          .groupBy { value =>
            val (topPerformanceStat, _) = value
            topPerformanceStat.value
          }
          .maxByOption { case (topPerformanceStat, _) =>
            topPerformanceStat
          }
          .flatMap { case (_, gamesWithTopPerformanceStat) =>
            gamesWithTopPerformanceStat
              .minByOption { case (topPerformanceChange, gameChanges) =>
                gameChanges
                  .find(minPredicate)
                  .map(_.value)
                  .getOrElse(0.0)
              }
          }
        def buildGreatPerformance(mainChange: InformationChangeParsed, changes: Iterable[InformationChangeParsed]) =
          GlobalStatisticsGreatestPerformance(
            mainChange.game,
            mainChange.hero,
            mainChange.timestamp,
            changes
              .find(_.stat.isGameCompletedCountResult)
              .map(_.assistant.isZero)
              .getOrElse(false),
            changes
              .find { change =>
                change.stat.isAssistCount &&
                  change.assistant.isHuman
              }
              .map(_.value.toInt)
              .getOrElse(0)
              .+(
                changes
                  .find(humanKillPredicate)
                  .map(_.value.toInt)
                  .getOrElse(0)
              ),
            changes
              .find(deathPredicate)
              .map(_.value.toInt)
              .getOrElse(0)
          )
        val singleGamesSorted = singleGames
          .toList
          .sortBy { case (timestamp, _) =>
            timestamp
          }
        val statsPerHero = boundedHistory
          .groupBy(_.hero)
          .toList
          .map { case (hero, changes) =>
            calculateDoubles(
              GlobalStatisticsElement(
                hero,
                fetchStatValue(changes)(humanKillPredicate),
                fetchStatValue(changes)(deathPredicate),
                fetchStatValue(changes)(humanAssistPredicate),
                fetchStatValue(changes)(winCountPredicate),
                fetchStatValue(changes)(looseCountPredicate)
              )
            )
          }
          .toSeq
          .sortBy(_.kills)
          .reverse
        GlobalStatisticsSubCategory(
          gameO.getOrElse(Game.Training),
          statsPerHero,
          Some(
            calculateDoubles(
              statsPerHero
                .foldLeft(GlobalStatisticsElement()) { case (acc, element) =>
                  acc.copy(
                    kills = acc.kills + element.kills,
                    deaths = acc.deaths + element.deaths,
                    assists = acc.assists + element.assists,
                    wins = acc.wins + element.wins,
                    losses = acc.losses + element.losses
                  )
                }
            )
          ),
          gameO.map { game =>
            GlobalStatisticsCurrentMode(
              countInDuels / totalDuelsCount,
              (countInDuels / gameToDuelsEq(game)).toInt
            )
          },
          findWithinSingleGamesMaxMinBy
          (
            singleGames,
            humanKillPredicate,
            deathPredicate
          )
            .map(Function.tupled(buildGreatPerformance)),
          findWithinSingleGamesMaxMinBy
          (
            singleGames,
            deathPredicate,
            humanKillPredicate
          )
            .map(Function.tupled(buildGreatPerformance)),
          singleGamesSorted
            .foldLeft(List.empty[List[(Long, Iterable[InformationChangeParsed])]]) { case (acc, value) =>
              val (timestamp, changes) = value
              (if(changes.hasWin()) {
                (if(acc.isEmpty) {
                  List.empty
                } else {
                  acc.init
                }) :+ (acc.lastOption.getOrElse(List.empty) :+ value)
              } else {
                acc :+ List.empty
              })
            }
            .filter(_.nonEmpty)
            .maxByOption(_.size)
            .map { longestStrike =>
              // safe because of filter above
              val (startsIn, _) = longestStrike.head
              val (endsIn, _) = longestStrike.last
              GlobalStatisticsStreak(
                longestStrike.size,
                longestStrike
                  .map { case (_, gameChanges) =>
                    gameChanges
                      .filter(humanKillPredicate)
                      .map(_.value.toInt)
                      .sum
                  }
                  .sum,
                longestStrike
                  .map { case (_, gameChanges) =>
                    gameChanges
                      .filter(deathPredicate)
                      .map(_.value.toInt)
                      .sum
                  }
                  .sum,
                startsIn,
                endsIn
              )
            }
        )
      }
      val groupedByTimestamp = groupByTimestamp(historyParsed.filter(_.qualifier.isPvp))
      val groupedByTimestampGame = groupedByTimestamp
        .map { case (season, changes) =>
          season -> changes.groupBy(_.game)
        }
      val gamesCountInDuels = groupedByTimestampGame
        .map { case (season, changesByGame) =>
          (
            season,
            changesByGame
              .map { case (game, changes) =>
                (
                  game,
                  changes
                    .collect({
                      case x if x.stat.isGameCompletedCountResult =>
                        x.value
                    }).size * gameToDuelsEq(game)
                )
              }
          )
        }
      val totalDuelsCountBySeason = gamesCountInDuels
        .map { case (season, countByGame) =>
          season -> countByGame.foldLeft(0.0) { case (acc, (game, changes)) =>
            acc + changes
          }
        }
      GlobalStatisticsReply(
        player.name,
        player.profile.id,
        player.trackingSource,
        player.activity.firstActivity.getOrElse(0L),
        toGlobalStatisticsActivityElements(player, {
          val takenActivity = historyParsed
            .filterNot(_.isFirstFetch)
            .sortBy(-_.timestamp)
            .toList
            .takeWhileWithAcc[Set[Long]](
              Set.empty,
              _.size < 11
            ) { case (change, acc) =>
              acc + change.timestamp
            }
          (takenActivity match {
            case Nil => List.empty
            case _ => takenActivity.init
          })
            .groupBy(_.timestamp)
        }),
        groupedByTimestampGame
          .toList
          .sortBy { case (seasonIndicator, _) =>
            seasonIndicator match {
              case Some(season) => season.number * -1
              case None => 0
            }
          }
          .map { case (seasonIndicator, changesByGame) =>
            val statsByGame = changesByGame
              .map { case (game, boundedHistory) =>
                // bounded by season and game
                createCategory(
                  Some(game),
                  gamesCountInDuels
                    .get(seasonIndicator)
                    .flatMap(_.get(game))
                    .getOrElse(0.0),
                  totalDuelsCountBySeason
                    .get(seasonIndicator)
                    .getOrElse(1.0),
                  boundedHistory
                )
              }
              .toSeq
              .sortBy(_.stats.map(_.playRate * - 1))
            val overall = createCategory(
              None,
              1.0,
              1.0,
              groupedByTimestamp.get(seasonIndicator).getOrElse(Seq.empty)
            )
            GlobalStatisticsCategory(
              seasonIndicator.map { season =>
                SeasonIndicator(
                  season.number,
                  season.title
                )
              },
              overall +: statsByGame
            )
          }
          .toSeq,
        Some(
          GlobalStatisticsAwards(
            allLeaderboardSeasons
              .reverse
              .flatMap { season =>
                leaderboardHistory
                  .filter { case (iterationTimestamp, history) =>
                    iterationTimestamp < season.endTimestamp(allLeaderboardSeasons)
                  }
                  .lastOption
                  .flatMap { case (iterationTimestamp, history) =>
                    val position = history.indexOf(player.profile.id)
                    if(position >= 0) {
                      Some(
                        GlobalStatisticsRankedAward(
                          season.number,
                          position + 1,
                          groupedByTimestamp
                            .get(Some(season))
                            .map { wholeSeasonChanges =>
                              findMostPlayedHero(wholeSeasonChanges.filter(rankedGamePredicate))
                            }
                            .getOrElse(Hero.HeroUnspecified)
                        )
                      )
                    } else {
                      None
                    }
                  }
              }
          )
        )
      )
    }
  }

}
