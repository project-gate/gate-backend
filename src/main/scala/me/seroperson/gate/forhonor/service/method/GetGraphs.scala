package me.seroperson.gate.forhonor.service.method

import akka.grpc.scaladsl.Metadata
import me.seroperson.gate.forhonor.model.cache.GraphCacheKey
import me.seroperson.gate.forhonor.service._

import scala.concurrent.Future

trait GetGraphs extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getGraphs(in: GraphsRequest, metadata: Metadata): Future[GraphsReply] = {
    for {
      selectedSeason <- Future.successful(allSeasons.find(_.number == in.season))
      (rateGraph, customLeaderboard) <- selectedSeason match {
        case Some(season) =>
          perSeasonCustomLeaderboardCache
            .get(GraphCacheKey(season))
            .map { result =>
              result
                .map { case (a, b) =>
                  (Some(a), Some(b))
                }
            }
            .getOrElse(Future.successful((None, None)))
        case None =>
          Future.successful((None, None))
      }
    } yield {
      GraphsReply(
        rateGraph,
        customLeaderboard
      )
    }
  }

}
