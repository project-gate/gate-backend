package me.seroperson.gate.forhonor.service.method

import akka.grpc.scaladsl.Metadata
import me.seroperson.gate.forhonor.service._

import scala.concurrent.Future

trait GetHistory extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getHistory(in: HistoryRequest, metadata: Metadata): Future[HistoryReply] = {
    getPlayersByRequest(in.id).flatMap { player =>
      getOrUpdateParsedHistory(player, in.firstTimestamp, in.lastTimestamp, in.skip, in.limit)
        .map { historyParsed =>
          val result = historyParsed
            .map { case(player, changes) =>
              toGlobalStatisticsActivityElements(player, changes)
            }
            .flatten
            .toList
            .sortBy(-_.timestamp)
          val hasNext = result.size <= in.limit
          HistoryReply(
            hasNext,
            in.skip + in.limit,
            result
          )
        }
    }
  }

}
