package me.seroperson.gate.forhonor.service.method

import akka.grpc.scaladsl.Metadata
import me.seroperson.gate.forhonor.service._

import scala.concurrent.Future

trait GetLeaderboardActivity extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getLeaderboardActivity(in: LeaderboardActivityRequest, metadata: Metadata): Future[LeaderboardActivityReply] = {
    (for {
      history <- leaderboardDaoService.count().flatMap { key =>
        leaderboardHistoryCache.getOrLoad(
          key,
          _ => leaderboardDaoService.composeLeaderboardHistory()
        )
      }
      iterationProfileIds = history
        .lastOption
        .map { case (_, profileIds) =>
          profileIds
        }
        .getOrElse(List.empty)
      players <- playerDaoService
        .findByProfileIds(iterationProfileIds)
        .map(
          _
            .map { player =>
              player.id -> player
            }
            .toMap
        )
      playersSeq = players.values.toSeq
      currentSeason <- allLeaderboardSeasons.lastOption match {
        case Some(value) => Future.successful(value)
        case None => Future.failed(new IllegalStateException("Unable to retrieve leaderboard activity as long as current season is not defined"))
      }
      fromTimestamp = currentSeason.startTimestamp
      toTimestamp = System.currentTimeMillis()
      matchHistoryByPlayer <- findCurrentRankedMatchHistoryByPlayer(playersSeq, fromTimestamp, toTimestamp)
      matchHistoryByTimestamp = {
        matchHistoryByPlayer
          .iterator
          .flatMap { case (playerId, changesByTimestamp) =>
            changesByTimestamp
              .iterator
              .map { case ((timestamp, changes)) =>
                (timestamp, playerId -> changes)
              }
          }
          .toList
          .sortBy { case (timestamp, _) =>
            - timestamp
          }
      }
    } yield {
      LeaderboardActivityReply(
        matchHistoryByTimestamp
          .takeWhileWithAcc[Int](0, _ < 100) { case ((_, (_, playerChanges)), acc) =>
            playerChanges.map(_.hero).distinct.size + acc
          }
          .flatMap { case(timestamp, (playerId, playerChanges)) =>
            players
              .get(playerId)
              .map { player =>
                val rankedChanges = matchHistoryByPlayer
                  .get(playerId)
                  .getOrElse(Map.empty)
                LeaderboardActivity(
                  timestamp,
                  player.name,
                  player.profile.id,
                  iterationProfileIds.indexOf(player.profile.id),
                  findMostPlayedHero(rankedChanges),
                  playerChanges
                    .groupBy(_.hero)
                    .toList
                    .map { case (hero, changes) =>
                      LeaderboardActivityElement(
                        hero,
                        changes
                          .filter(_.assistant.isUno)
                          .map(_.value)
                          .sum
                          .toInt,
                        changes
                          .filter(_.assistant.isZero)
                          .map(_.value)
                          .sum
                          .toInt
                      )
                    }
                    .toSeq
                )
              }
          }
      )
    })
  }

}
