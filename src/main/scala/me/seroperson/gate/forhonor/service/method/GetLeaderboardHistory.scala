package me.seroperson.gate.forhonor.service.method

import akka.grpc.scaladsl.Metadata
import me.seroperson.gate.forhonor.service._

import scala.concurrent.Future

trait GetLeaderboardHistory extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getLeaderboardHistory(in: LeaderboardHistoryRequest, metadata: Metadata): Future[LeaderboardHistoryReply] = {
    (for {
      history <- leaderboardDaoService.count().flatMap { key =>
        leaderboardHistoryCache.getOrLoad(
          key,
          _ => leaderboardDaoService.composeLeaderboardHistory()
        )
      }
      iteration <- history
        .find { case (timestamp, _) =>
          timestamp == in.timestamp
        }
        .orElse(history.lastOption) match {
          case Some(value) => Future.successful(value)
          case None => Future.failed(new IllegalArgumentException)
        }
      (selectedIterationTimestamp, iterationProfileIds) = iteration
      nowTimestamp = System.currentTimeMillis()
      isLastIteration = history.lastOption.exists(_ == iteration)
      firstSeason <- allLeaderboardSeasons.headOption match {
        case Some(value) => Future.successful(value)
        case None => Future.failed(new IllegalStateException("Unable to retrieve leaderboard history as long as current season is not defined"))
      }
      currentSeason <- allLeaderboardSeasons.lastOption match {
        case Some(value) => Future.successful(value)
        case None => Future.failed(new IllegalStateException("Unable to retrieve leaderboard history as long as current season is not defined"))
      }
      selectedSeason <- Future.successful(if(selectedIterationTimestamp > 0) {
        if(firstSeason.startTimestamp > selectedIterationTimestamp) {
          firstSeason
        } else {
          allLeaderboardSeasons
            .find { season =>
              season.startTimestamp <= selectedIterationTimestamp && season.endLeaderboardTimestamp(allLeaderboardSeasons) > selectedIterationTimestamp
            }
            .getOrElse(currentSeason)
        }
      } else {
        currentSeason
      })
      (currentSeasonTimestamp, seasonNumber, nextSeasonTimestamp) = {
        (selectedSeason.startLeaderboardTimestamp, selectedSeason.number, selectedSeason.endLeaderboardTimestamp(allLeaderboardSeasons))
      }
      seasonRankedProfileIds = history
        .filter { case (timestamp, _) =>
          timestamp >= currentSeasonTimestamp && timestamp < nextSeasonTimestamp
        }
        .map { case (_, profileIds) =>
          profileIds
        }
        .flatten
        .distinct
      seasonRankedPlayers <- pastLeaderboardPlayersCache
        .getOrLoad(seasonNumber, _ => playerDaoService.findByProfileIds(seasonRankedProfileIds))
        .map(
          _
            .map { player =>
              player.profile.id -> player
            }
            .toMap
        )
      matchHistoryByPlayer <- {
        if(isLastIteration) {
          val iterationPlayers = seasonRankedPlayers
            .filter { case (profileId, player) =>
              val (_, iterationProfileIds) = iteration
              iterationProfileIds.contains(profileId)
            }
          findCurrentRankedMatchHistoryByPlayer(
            iterationPlayers.values.toSeq,
            currentSeason.startTimestamp,
            nowTimestamp
          )
        } else {
          pastLeaderboardInformationCache
            .getOrLoad(
              seasonNumber,
              cacheKey =>
                historyDaoService
                  .findLiveCaptured(rankedKeys, seasonRankedPlayers.values.toSeq, currentSeasonTimestamp, nextSeasonTimestamp)
            )
            .map { result =>
              result
                .view
                .mapValues { allPlayerRankedChanges =>
                  allPlayerRankedChanges
                    .view
                    .filterKeys(_ < selectedIterationTimestamp)
                    .toMap
                }
                .toMap
            }
        }
      }
      nextIteration = (history
        .dropWhile { case (elementTimestamp, _) =>
          elementTimestamp != selectedIterationTimestamp
        } match {
          case Nil => List.empty
          case list => list.tail
        })
        .headOption
      previousIteration = history
        .takeWhile { case (elementTimestamp, _) =>
          elementTimestamp != selectedIterationTimestamp
        }
        .lastOption
    } yield {
      LeaderboardHistoryReply(
        Some(
          LeaderboardHistoryIteration(
            selectedIterationTimestamp,
            iterationProfileIds
              .zipWithIndex
              .flatMap { case (profileId, index) =>
                seasonRankedPlayers
                  .get(profileId)
                  .map { player =>
                    val playerId = player.id
                    val rankedChanges = matchHistoryByPlayer
                      .get(playerId)
                      .getOrElse(Map.empty)
                    val previousPosition = previousIteration
                      .flatMap { case (iterationTimestamp, profileIds) =>
                        if(iterationTimestamp < currentSeasonTimestamp) {
                          None
                        } else {
                          profileIds.indexOf(profileId) match {
                            case -1 => None
                            case exists => Some(exists + 1)
                          }
                        }
                      }
                      .getOrElse(-1)
                    val lastRankedActivity = if (rankedChanges.isEmpty) {
                      0L
                    } else {
                      if(isLastIteration) {
                        rankedChanges
                          .maxBy { case (timestamp, _) =>
                            timestamp
                          }
                          ._1
                      } else {
                        val pastTimestamp = rankedChanges
                          .filter { case (timestamp, _) =>
                            timestamp <= selectedIterationTimestamp
                          }
                          .maxBy { case (timestamp, _) =>
                            timestamp
                          }
                          ._1
                        nowTimestamp - (selectedIterationTimestamp - pastTimestamp)
                      }
                    }
                    val topRankedHero = findMostPlayedHero(rankedChanges)
                    LeaderboardHistoryIterationElement(
                      index + 1,
                      previousPosition,
                      player.name,
                      player.profile.id,
                      lastRankedActivity,
                      topRankedHero
                    )
                  }
              }
          )
        ),
        seasonNumber,
        nextIteration
          .map { case (elementTimestamp, _) =>
            elementTimestamp
          }
          .getOrElse(0),
        previousIteration
          .map { case (elementTimestamp, _) =>
            elementTimestamp
          }
          .getOrElse(0),
        selectedSeason
          .nextSeason(allLeaderboardSeasons)
          .flatMap { nextSeason =>
            findSeasonFirstIteration(history, nextSeason)
          }
          .getOrElse(0L),
        selectedSeason // todo: skipping some season is not available for now
          .previousSeason(allLeaderboardSeasons)
          .flatMap { previousSeason =>
            findSeasonFirstIteration(history, previousSeason)
              .filter { iterationCandidate =>
                iterationCandidate < previousSeason.endLeaderboardTimestamp(allLeaderboardSeasons)
              }
          }
          .getOrElse(0L)
      )
    })
  }

}
