package me.seroperson.gate.forhonor.service.method

import akka.grpc.scaladsl.Metadata
import me.seroperson.gate.forhonor.model.Season._
import me.seroperson.gate.forhonor.model.cache.GraphCacheKey
import me.seroperson.gate.forhonor.service._

import scala.concurrent.Future

trait GetPlayers extends ServicePowerApiArgs with MainServicePowerApi with Shared {

  override def getPlayers(in: PlayersRequest, metadata: Metadata): Future[PlayersReply] = {
    for {
      players <- playerDaoService.findAll()
      playerElements = playersToPlayerElements(players)
      selectedSeason = lastSeason
      (rateGraph, customLeaderboard) <- selectedSeason match {
        case Some(season) =>
          perSeasonCustomLeaderboardCache
            .get(GraphCacheKey(season))
            .map { result =>
              result
                .map { case (a, b) =>
                  (Some(a), Some(b))
                }
            }
            .getOrElse(Future.successful((None, None)))
        case None =>
          Future.successful((None, None))
      }
    } yield {
      val previousSeason = selectedSeason.flatMap(_.previousSeason(allSeasons).map(_.number)).getOrElse(-1)
      PlayersReply(
        selectedSeason.map { season =>
          SeasonIndicator(
            season.number,
            season.title
          )
        },
        allSeasons.map { season =>
          SeasonIndicator(
            season.number,
            season.title
          )
        },
        playerElements,
        rateGraph,
        customLeaderboard
      )
    }
  }
}
