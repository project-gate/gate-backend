package me.seroperson.gate.forhonor.service.method

import akka.grpc.GrpcServiceException
import akka.grpc.scaladsl.Metadata
import io.grpc.Status
import me.seroperson.gate.forhonor.behavior.model.InformationChangeParsed
import me.seroperson.gate.forhonor.model.{Game, Hero, StoredClan}
import me.seroperson.gate.forhonor.service._

import scala.concurrent.Future

trait Og extends ServicePowerApiArgs with OgServicePowerApi with Shared {

  override def getPlayers(in: OgPlayersRequest, metadata: Metadata): Future[OgPlayersReply] = {
    playerDaoService
      .findAll()
      .map { players =>
        OgPlayersReply(
          players.size
        )
      }
  }

  override def getPlayer(in: OgPlayerRequest, metadata: Metadata): Future[OgPlayerReply] = {
    for {
      player <- getPlayerByIdentity(in.id)
      historyParsedTotal <- getOrUpdateParsedHistory(player)
    } yield {
      val historyParsedPvp = historyParsedTotal.filter(_.qualifier.isPvp)
      val totalKills = historyParsedPvp
        .collect {
          case x if InformationChangeParsed.humanKillPredicate(x) =>
            x.value
        }
        .sum
      val totalDeaths = historyParsedPvp
        .collect {
          case x if InformationChangeParsed.deathPredicate(x) =>
            x.value
        }
        .sum

      val groupedByGame = historyParsedPvp
        .groupBy(_.game)

      val mostPlayedMode = groupedByGame
        .map { case (game, changes) =>
          (
            game,
            changes
              .collect({
                case x if x.stat.isGameCompletedCountResult =>
                  x.value
              }).size * gameToDuelsEq(game)
          )
        }
        .maxByOption { case (_, count) =>
          count
        }
        .map { case (game, _) =>
          game
        }

      val mostPlayedHero = groupedByGame
        .map { case (game, changes) =>
          changes
            .groupBy(_.hero)
            .map { case (hero, heroChanges) =>
              (
                hero,
                heroChanges
                  .collect({
                    case x if x.stat.isGameCompletedCountResult =>
                      x.value
                  }).size * gameToDuelsEq(game)
              )
            }
        }
        .foldLeft(Map.empty[Hero, Double]) { case (acc, heroPopularityInMode) =>
          heroPopularityInMode
            .foldLeft(acc) { case (innerAcc, (hero, popularity)) =>
              innerAcc.updated(hero, innerAcc.getOrElse(hero, 0.0) + popularity)
            }
        }
        .maxByOption { case (_, count) =>
          count
        }
        .map { case (hero, _) =>
          hero
        }

      OgPlayerReply(
        player.name,
        player.profile.id,
        totalKills / (if(totalDeaths == 0) {
          -1.0
        } else {
          totalDeaths
        }),
        mostPlayedMode.getOrElse(Game.GameUnspecified),
        mostPlayedHero.getOrElse(Hero.HeroUnspecified)
      )
    }
  }

  override def getClan(in: OgClanRequest, metadata: Metadata): Future[OgClanReply] = {
    for {
      clan <- clanDaoService
        .findByNameId(in.id)
        .flatMap {
          _
            .fold[Future[StoredClan]](
              Future.failed(
                new GrpcServiceException(
                  Status.INVALID_ARGUMENT
                    .withDescription("Unknown clan identifier.")
                )
              )
            )(Future.successful)
        }
    } yield {
      OgClanReply(
        clan.nameId,
        clan.nameView,
        clan.image.getOrElse(""),
        clan.shortDescription,
        clan.country
      )
    }
  }

  override def getLeaderboard(in: OgLeaderboardRequest, metadata: Metadata): Future[OgLeaderboardReply] = {
    Future.successful(OgLeaderboardReply())
  }
}
