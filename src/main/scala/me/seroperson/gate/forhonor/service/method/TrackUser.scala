package me.seroperson.gate.forhonor.service.method

import akka.actor.typed.scaladsl.AskPattern._
import akka.grpc.GrpcServiceException
import akka.grpc.scaladsl.Metadata
import io.grpc.Status
import me.seroperson.gate.forhonor.behavior.RequestMaker
import me.seroperson.gate.forhonor.model.Player
import me.seroperson.gate.forhonor.service.{MainServicePowerApi, ServicePowerApiArgs, TrackUserReply, TrackUserRequest}

import scala.concurrent.Future

trait TrackUser extends ServicePowerApiArgs with MainServicePowerApi {

  override def trackUser(in: TrackUserRequest, metadata: Metadata): Future[TrackUserReply] = {
    val name = in.name
    val profileResponse: Future[Set[Player]] =
      (requestMaker ? (x => RequestMaker.ProfileIdsByNames(platform, Seq(name), Player.SourceTrackUser, x)))
    profileResponse
      .map(_.headOption)
      .flatMap { response =>
        response match {
          case Some(player) =>
            Future.successful(player)
          case None =>
            Future.failed(
              new GrpcServiceException(
                Status.ABORTED
                  .withDescription("Unable to retrieve response from ubisoft. Possibly such player is not exists.")
              )
            )
        }
      }
      .flatMap { player =>
        playerDaoService.insertIfNothing(player)
          .map { stored =>
            TrackUserReply(
              stored.name,
              stored.profile.id
            )
          }
          .recoverWith { case ex =>
            Future.failed(
              new GrpcServiceException(
                Status.ABORTED
                  .withDescription("Unable to connect to database.")
              )
            )
          }
      }
  }

}
