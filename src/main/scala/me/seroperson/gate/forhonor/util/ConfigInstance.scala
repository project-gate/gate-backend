package me.seroperson.gate.forhonor.util

import com.typesafe.config.ConfigFactory

object ConfigInstance {
  private val DefaultValues = ConfigFactory.load("reference")
  private val Config = ConfigFactory.load().withFallback(DefaultValues)
  val Http = Config.getConfig("http")
  val MongoDb = Config.getConfig("mongodb")
  val Logstash = Config.getConfig("logstash")
  val UbiServices = Config.getConfig("ubi-services")
  object Webhook {
    val Root = Config.getConfig("webhook")
    val Discord = Config.getConfig("webhook.discord")
  }
  object Gate {
    val Root = Config.getConfig("gate")
    val Tracker = Config.getConfig("gate.tracker")
    val TrackerLeaderboard = Tracker.getConfig("leaderboard")
    val TrackerNames = Tracker.getConfig("names")
    val TrackerGraph = Tracker.getConfig("graph")
  }
}
