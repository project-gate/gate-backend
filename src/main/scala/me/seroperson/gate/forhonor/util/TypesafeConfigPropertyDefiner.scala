package me.seroperson.gate.forhonor.util

import ch.qos.logback.core.PropertyDefinerBase

import scala.beans.BeanProperty

class TypesafeConfigPropertyDefiner extends PropertyDefinerBase {
  @BeanProperty
  var path: String = _
  override def getPropertyValue = ConfigInstance.Logstash.getAnyRef(path).toString
}
