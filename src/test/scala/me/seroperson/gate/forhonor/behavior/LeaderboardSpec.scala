package me.seroperson.gate.forhonor.behavior

import me.seroperson.gate.forhonor.behavior.model.LeaderboardChangeExtended._
import org.scalatest.FlatSpec

class LeaderboardSpec extends FlatSpec {

  val initial100 = List(
    "d7f1fdc4-82bf-4059-b344-ba79371aee8e",
    "9643fd4a-7db5-4f26-a781-c26d82c1e444",
    "f1700e4a-768c-4369-9fa1-5e4ce74fde3f",
    "9fb899f5-5aef-4b1a-8fe2-3e215c630225",
    "2dc24e9c-a747-4485-a5cb-f628c86f01d2",
    "34e1d286-46f5-4341-9903-b415f6541c31",
    "a1dbd95a-b315-419f-832b-69e24be82f70",
    "19c945b3-59e9-413e-8b0d-b97c65227f41",
    "564eb59d-2f78-4df9-8aca-e164300670ba",
    "b3e8e2e2-6ede-42df-9c14-5705159004d4",
    "c927acf6-7ac7-4911-ab08-568eac43870e",
    "82ff4893-d621-4335-994d-c23457b1fd69",
    "1d1a64b1-db76-4abb-a39e-59961985ff8a",
    "a283aa87-119a-4637-830b-39930478f88b",
    "12154143-f2ab-4fb1-8d08-6a2b73d71411",
    "43236ede-44ee-48a8-b2cb-48729d68201c",
    "869c15d6-dfaf-43b6-882d-ba03835c40d4",
    "3caad15c-9578-4395-964c-701594dc14d7",
    "a5897f42-bd6b-4744-9f55-fcc618c5d0f1",
    "98c80c71-e2cd-4313-85ba-1da2ea2daf1c",
    "adc4a13f-c58c-4918-afec-7ac62cdc2b25",
    "74d19728-fc76-4088-99aa-5cc61767bb0a",
    "0a448f01-9413-4059-872d-819a39493b3b",
    "5de1fbe3-4553-427f-9e25-353f84699e62",
    "9b6d478c-be9c-41f7-a501-f26ceff1505f",
    "502ddedd-3e64-4f5f-a9aa-83069e8aa995",
    "e4e4afd4-b012-443d-85db-0f6f06647469",
    "5bb9ebe0-f25e-4c8a-8cd1-1cdc3f6771a0",
    "bdaa932d-a267-4f1a-914e-550d9267b163",
    "132e9365-843a-4b1d-b0e9-c86f3d903031",
    "63446e5f-ad8e-418b-a961-d03701c40976",
    "6a3912a5-3767-4ca8-98b1-be420c61161f",
    "1afb54b3-40e4-4c51-bf7b-9183027bca6f",
    "e3c4d39b-67e0-4a44-acd9-a6f55cc1d7f6",
    "acffbce5-db7e-44c8-a084-f7582a3c6e7f",
    "e5b845b2-b9b1-41a6-885d-6e40e3015cf8",
    "e837ec2e-f8a7-4d81-b5ec-db9b507239c2",
    "4114190c-21cb-4270-a2bf-0b95d600846b",
    "ae3d2fb5-0ab2-445a-80e1-656b5759966e",
    "f45116ad-835a-4c1b-ba30-7bebd98f4dc9",
    "a3d003c7-49cf-4917-822c-dea7753f3093",
    "89fb0dc5-08bd-4f1f-b6cd-9cbd16184002",
    "6405d548-cbcd-4f96-a50b-b0732e57f56d",
    "04061480-d747-49ed-931f-c2209d185fef",
    "154c7c2b-20fb-4f3a-8ad7-36ac2ae8848f",
    "c0c08a91-c277-4b5a-a1ee-034c2a909d61",
    "af53018e-fbc7-4da5-9f6e-000a1373688a",
    "d6dcc4b4-1194-468f-8577-8e783ff70405",
    "9cdbf31b-937f-434b-9052-7d7e1d90fb07",
    "cab6572b-6cb4-4c85-9531-33b466444602",
    "583ed5ff-6a8b-421e-aed4-1caf70da6e2a",
    "05cd5832-82ce-48ed-a57b-465cb25c942f",
    "3b211377-5fae-4e93-bf2a-3b92fb135edf",
    "8d065728-b715-4273-9170-a91183ec5ece",
    "078c0032-f069-4340-886f-3eafc17b845a",
    "386575a0-e76b-4e6a-9d38-29d78362a69f",
    "5c397228-fdd5-4939-84fc-77711f70405c",
    "f88c2f42-0c42-4c1d-bfb2-9ab274b77d13",
    "94d00eca-b4ca-40f2-9594-86b45bec15b4",
    "68c8b4a0-5c49-46d3-b045-bb5931588e11",
    "d0744d59-b702-4305-81f2-081f44e25ab4",
    "c099fcfa-067e-4c39-9182-cca444aa8b89",
    "39be7a01-1e0a-4f7b-8869-518919075946",
    "e1c1f9af-31ad-4ad9-b127-94d60617da8e",
    "acf5bf36-27bd-47ec-b372-215e7d54d587",
    "0ed6a268-b5e8-4e7d-8ec8-e1802adc782d",
    "1fc1893d-65d1-4376-addf-0bf9243e0ab0",
    "977319ec-8755-489e-a333-99d63597a7ac",
    "158ba7fa-2fe8-4bfd-b78a-220b41421654",
    "433ef572-cf00-4c8e-af45-09a6a2fda5a6",
    "3dd2b573-cccf-4359-b0ed-61d73c534155",
    "433ef572-cf00-4c8e-af45-09a6a2fda5a6",
    "0d328cb1-17a0-4570-af01-9c9dc940a96e",
    "4bf34250-1360-4297-a0aa-ef661bc1334e",
    "dbebe1f0-b7c1-48c3-8337-b7f6d2157187",
    "c747e278-3ed3-4838-a204-86ff497e946f",
    "0d7f6803-ebd7-4eaa-98ca-de85e1023bf2",
    "0ee5433c-5135-4783-98b5-02d089c0e9d7",
    "08d12bb5-51a2-4c67-bfaa-730cadb5765d",
    "f69ad9db-73f4-4760-b623-3feb67a1dca3",
    "e099982f-ac97-44ac-ad60-22d6d3a5e703",
    "b06b1ca0-521a-4098-84b0-91f12130a975",
    "21b0512f-351d-40ef-94f2-9c4e1c0b62eb",
    "51416d17-1066-4ad7-b82c-08f04f9f921c",
    "5f6f83d7-1ecc-4d4b-9e5b-ff0271102b41",
    "d1cd7c0e-81cc-420b-b18f-b64eb2c2fb9a",
    "2e2e7004-1250-4632-97db-81b63c7e24ed",
    "b1c80630-660c-455b-8b59-33bc568d6b85",
    "6053e33f-3ad6-46f2-91a8-b8ffcc664407",
    "3e8d366f-e8d2-4344-a68f-e1e7c10d4e04",
    "60306575-b820-4018-b38a-494b2498b8eb",
    "004bffbc-7b18-4f86-99f3-d69a341c11f8",
    "c1e34df0-6ce0-467d-84b0-a5a549daae2f",
    "c8c26219-8d9d-4123-9036-faadb9b3f63b",
    "293f35a8-741a-4814-987d-043e3a065727",
    "6f32476f-154e-4ec4-b886-fbfb3181214e",
    "d278c306-646b-489a-bf76-c08e16aba7cf",
    "9a9b135b-649e-4fe2-88bf-ed26d3ce1f62",
    "5637a85e-e8fb-447f-97a6-e2106cfbf04a",
    "c31feaa2-4576-4f12-a73c-f17439b57101"
  )
  val intial64 = List(
    "4b1e9ec2-60d2-4053-86e6-4b7f4371ea93",
    "4e48e91b-ad47-44b8-a2aa-b77d973c3e2d",
    "346bae8b-32df-4a1b-8625-8a5109a76fef",
    "cab6572b-6cb4-4c85-9531-33b466444602",
    "94d00eca-b4ca-40f2-9594-86b45bec15b4",
    "e4e4afd4-b012-443d-85db-0f6f06647469",
    "9e1c0320-d0d0-486a-b245-e655b267d3fc",
    "6751ebdb-0f07-4145-b648-9af92a844a32",
    "cdd9521a-4e85-4617-ad20-77be7a572f49",
    "fd1b1e4d-f22b-4c09-9246-752e05d03a25",
    "9648a25a-f3c5-47ec-a3eb-df9e98dbdbd6",
    "9b33f107-80b9-4691-91de-fc1c09a5d08e",
    "b03706a7-70ab-4d0e-9b98-54a7eaa90b77",
    "a61866c0-0697-4327-b22d-a1fae7c34aa7",
    "b1c80630-660c-455b-8b59-33bc568d6b85",
    "9d632266-82fe-4e9c-94fa-ddede0746c99",
    "de750e8b-cbc9-497f-bc11-074817b3b43e",
    "60044cc9-55b7-44f8-a128-235c7b63e715",
    "2030ff37-119d-4ef5-9597-6f2033ec94e6",
    "9dc3c117-4f41-49c0-977b-529898204672",
    "7a0de2fe-cc2b-475d-9fbf-f13f0417cd60",
    "8981cd9c-f2cd-4a01-842a-f30808d0fd62",
    "8f7d06e2-0aea-4452-8d56-51f0322c9a94",
    "bd1a4aed-0434-4738-a7ab-31aff99a1207",
    "25311c95-b012-4222-8062-282e7259368b",
    "49817528-445a-4fc4-af49-0fb8104d34a9",
    "85623521-abb4-44f7-b286-0e053abc0670",
    "308c27c3-026e-45d2-a93a-8fc0838b42ce",
    "e684a6ba-1c01-4dda-ad59-a790173d2936",
    "dddb8128-0906-47f2-a14e-dcb8025525a3",
    "6af40f2b-8773-4ba5-b851-c77b82b17579",
    "2d3784ad-c635-460a-96b2-ccaa7ffef516",
    "b386225e-a3dd-423f-8388-c2c010f1cdd5",
    "e3e2dfcb-003a-44e5-9a44-af0497fb15fb",
    "4eeef2e5-e889-444b-a1dc-28996366ec9a",
    "0c63ff84-e07a-41f3-9389-0e8c2124d8ba",
    "9b7abf0c-e835-41ae-a149-83cd42d50613",
    "8c0e1aba-fce7-4846-b7ca-c9bf01d7e0ef",
    "f92db8c3-1437-408f-adbb-6da83fc02ed5",
    "2a3a89b1-df8d-4727-ab8c-981c4d91457b",
    "13efbbf5-838f-44e3-a010-fba542cd7fe2",
    "8a50a425-53bf-42df-bbb2-d1983f3bbbec",
    "0439eff7-8e0d-43d4-8661-ecec532f9b95",
    "d9e5dc92-baae-43a1-bf01-e864e18224f7",
    "7f148f34-982d-48b3-8dc8-7649e58ff07d",
    "459e7dd0-b57e-4b6c-8c0e-03f0f21c7908",
    "c30d01ad-c242-4670-97d7-4bd0ade9de78",
    "d5e3b3a2-b327-478b-8247-23104aea0dbd",
    "8a55bbce-def0-45b3-8557-0c9fee9bba1b",
    "e16b4cbb-e979-4064-b457-a01bc9e5d499",
    "840c9cbb-781b-4c1c-bc90-19d6ab8a0fa4",
    "19206a40-1140-453b-85f6-9d232c2b1080",
    "f89228f5-f7c9-46a3-b997-4b32fa010610",
    "777cf2a9-c07d-4458-8f4a-d1228076a313",
    "b85f4e36-9391-4e48-82c1-a8631b34d66e",
    "0b7f35c1-90df-4ac3-bf23-cdf95ead5b05",
    "fb69f7af-0a79-404d-86a9-7caea2d65622",
    "1cd46e4b-9599-4acf-8bd1-944ea8b5c7fa",
    "924a471e-610c-4391-b79e-93fcf611bce5",
    "d12fc609-3108-4be4-b10c-3320612a05c6",
    "e0a5768c-c432-4b85-9e03-47ad2fb5b8a5",
    "20e414cc-1004-4968-a2f8-a68f80c31906",
    "e348e82f-691b-4cae-aba2-4ec562b848c3",
    "0619f115-d754-402e-99ef-68f73e428e96"
  )
  val initial0 = List(
  )
  val initial1 = List(
    "0619f115-d754-402e-99ef-68f73e428e96"
  )

  "LeaderboardChangeExtended" should "find diff correctly if old.size > new.size and there are a lot of changes" in {
    initial100.diffWith(intial64)
  }

  "LeaderboardChangeExtended" should "apply diff correctly if old.size > new.size and there are a lot of changes" in {
    val diff = initial100.diffWith(intial64)
    val result = initial100.applyDiff(diff)
    assert(intial64 == result)
  }

  "LeaderboardChangeExtended" should "find diff correctly if old.size == 0" in {
    initial0.diffWith(initial1)
  }

  "LeaderboardChangeExtended" should "apply diff correctly if old.size == 0" in {
    val diff = initial0.diffWith(initial1)
    val result = initial0.applyDiff(diff)
    assert(initial1 == result)
  }

}
